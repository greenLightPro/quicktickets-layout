	<meta charset="utf-8">
    <title>QUICK TICKETS | Билетная система для учреждений культуры</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
    <meta name="description" content=""/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="stylesheet" href="css/jquery.scrollbar.css" type="text/css">
	<link rel="stylesheet" href="css/lightgallery.css" type="text/css">
	<link rel="stylesheet" href="css/style.css?t=<?php echo(microtime(true).rand()); ?>" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/fonts.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">



    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <meta property="og:description" content="Государственные театры и филармонии переходят на Quick Tickets">
    <meta property="og:image" content="https://hi-lingo.com/projects/theater/screenshot.jpg">
    <meta property="og:site_name" content="quicktickets.ru">
    <meta property="og:title" content="QUICK TICKETS | Билетная система для учреждений культуры">
    <meta property="og:type" content="website">






