<div id="modal_overlay" class="modal_overlay"></div>

<div class="modal_wrapper" id="modal-send-form">
    <div class="modal_block user-modal-block">
        <div class="btn-close  modal_close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 409.806 409.806" style="enable-background:new 0 0 409.806 409.806;" xml:space="preserve">
		            <path d="M228.929,205.01L404.596,29.343c6.78-6.548,6.968-17.352,0.42-24.132c-6.548-6.78-17.352-6.968-24.132-0.42
                        c-0.142,0.137-0.282,0.277-0.42,0.42L204.796,180.878L29.129,5.21c-6.78-6.548-17.584-6.36-24.132,0.42
                        c-6.388,6.614-6.388,17.099,0,23.713L180.664,205.01L4.997,380.677c-6.663,6.664-6.663,17.468,0,24.132
                        c6.664,6.662,17.468,6.662,24.132,0l175.667-175.667l175.667,175.667c6.78,6.548,17.584,6.36,24.132-0.42
                        c6.387-6.614,6.387-17.099,0-23.712L228.929,205.01z"/>
            </svg>
        </div>

        <p class="modal-title">
            <b>Заполните форму</b> и мы <br>
            перезвоним в первую <br>
            свободную минуту!
        </p>

        <form class="user-form send-form">
            <div class="input-item">
                <p class="input-label">Ваше учреждение:</p>
                <label class="user-input">
                    <input type="text" name="organisation" class="req">
                </label>
            </div>
            <div class="input-item">
                <p class="input-label">Как к Вам можно обращаться:</p>
                <label class="user-input">
                    <input type="text" name="name" class="req">
                </label>
            </div>
            <div class="input-item">
                <p class="input-label">Как с Вами связаться:</p>
                <label class="user-input">
                    <input type="tel" name="phone" placeholder="+7(___) ___-__-__" class="req">
                </label>
            </div>
            <div class="btn-wrap">
                <button type="submit" class="btn-user btn-submit"><span class="btn-inner">Отправить</span></button>
            </div>

            <p class="form-confidentiality-text">Я ознакомился с <a href="#" target="_blank">политикой конфиденциальности</a>
                и даю согласие на обработку данных
            </p>

            <input type="hidden" name="form_source" class="input-form-source">
        </form>
    </div>
</div>


<div class="modal_wrapper" id="modal-lead-magnet">
    <div class="modal_block user-modal-block modal-lead-magnet">
        <div class="btn-close  modal_close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 409.806 409.806" style="enable-background:new 0 0 409.806 409.806;" xml:space="preserve">
		            <path d="M228.929,205.01L404.596,29.343c6.78-6.548,6.968-17.352,0.42-24.132c-6.548-6.78-17.352-6.968-24.132-0.42
                        c-0.142,0.137-0.282,0.277-0.42,0.42L204.796,180.878L29.129,5.21c-6.78-6.548-17.584-6.36-24.132,0.42
                        c-6.388,6.614-6.388,17.099,0,23.713L180.664,205.01L4.997,380.677c-6.663,6.664-6.663,17.468,0,24.132
                        c6.664,6.662,17.468,6.662,24.132,0l175.667-175.667l175.667,175.667c6.78,6.548,17.584,6.36,24.132-0.42
                        c6.387-6.614,6.387-17.099,0-23.712L228.929,205.01z"/>
            </svg>
        </div>

        <div class="text-content">
            <p> <b>Подождите!</b></p>
            <p class="modal-big-text">
                <b>Пройдите интересный тест для сотрудников театров
                    и филармоний,</b> чтобы узнать какие возможности для увеличения посещаемости упускает ваше учреждение
            </p>
        </div>

        <div class="btn-wrap">
            <a href="https://vk.com/app6656524_-165981194" target="_blank" class="btn-user btn-submit btn"><span class="btn-inner">Пройти тест</span></a>
        </div>
    </div>
</div>

<div class="modal_wrapper" id="modal-final-send">
    <div class="modal_block user-modal-block modal-final-send">
        <div class="btn-close  modal_close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 409.806 409.806" style="enable-background:new 0 0 409.806 409.806;" xml:space="preserve">
		            <path d="M228.929,205.01L404.596,29.343c6.78-6.548,6.968-17.352,0.42-24.132c-6.548-6.78-17.352-6.968-24.132-0.42
                        c-0.142,0.137-0.282,0.277-0.42,0.42L204.796,180.878L29.129,5.21c-6.78-6.548-17.584-6.36-24.132,0.42
                        c-6.388,6.614-6.388,17.099,0,23.713L180.664,205.01L4.997,380.677c-6.663,6.664-6.663,17.468,0,24.132
                        c6.664,6.662,17.468,6.662,24.132,0l175.667-175.667l175.667,175.667c6.78,6.548,17.584,6.36,24.132-0.42
                        c6.387-6.614,6.387-17.099,0-23.712L228.929,205.01z"/>
            </svg>
        </div>

        <div class="text-content">
            <p><b>Спасибо за интерес!</b></p>
            <p>
                В ближайшее время мы с Вами свяжемся.
                Пока можете заглянуть к нам
                в паблик, в котором мы делимся интересным контентом для театров и филармоний, а также опытом наших друзей.
            </p>
            <p class="m-top"><b><a href="https://vk.com/qtstart" target="_blank">https://vk.com/qtstart</a></b></p>
            <p><b>Будем рады Вас видеть!</b></p>

        </div>
    </div>
</div>


<div class="modal_wrapper" id="modal-join">
    <div class="modal_block user-modal-block modal-join">
        <div class="btn-close  modal_close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 409.806 409.806" style="enable-background:new 0 0 409.806 409.806;" xml:space="preserve">
		            <path d="M228.929,205.01L404.596,29.343c6.78-6.548,6.968-17.352,0.42-24.132c-6.548-6.78-17.352-6.968-24.132-0.42
                        c-0.142,0.137-0.282,0.277-0.42,0.42L204.796,180.878L29.129,5.21c-6.78-6.548-17.584-6.36-24.132,0.42
                        c-6.388,6.614-6.388,17.099,0,23.713L180.664,205.01L4.997,380.677c-6.663,6.664-6.663,17.468,0,24.132
                        c6.664,6.662,17.468,6.662,24.132,0l175.667-175.667l175.667,175.667c6.78,6.548,17.584,6.36,24.132-0.42
                        c6.387-6.614,6.387-17.099,0-23.712L228.929,205.01z"/>
            </svg>
        </div>

        <div class="text-content">
            <p><b>Спасибо за интерес!</b></p>
            <p>
                В этом регионе у нас пока
                нет&nbsp;друзей.
            </p>
            <p class="m-top">Наши друзья получают множество преимуществ.</p>
            <p><b>Присоединяйтесь!</b></p>
        </div>

        <div class="btn-wrap modal-join-btn-wrap">
            <a href="javascript:void(0)" class="btn-user btn-submit btn-get-modal btn" data-id="#modal-send-form" data-source="Стать первым партнером"><span class="btn-inner">Стать первым партнером</span></a>
        </div>
    </div>
</div>


<div class="modal_wrapper" id="geo-label-region">
    <div class="modal_block_region geo-label-modal geo-label-shadow ">
        <div class="btn-close  modal_close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 409.806 409.806" style="enable-background:new 0 0 409.806 409.806;" xml:space="preserve">
		            <path d="M228.929,205.01L404.596,29.343c6.78-6.548,6.968-17.352,0.42-24.132c-6.548-6.78-17.352-6.968-24.132-0.42
                        c-0.142,0.137-0.282,0.277-0.42,0.42L204.796,180.878L29.129,5.21c-6.78-6.548-17.584-6.36-24.132,0.42
                        c-6.388,6.614-6.388,17.099,0,23.713L180.664,205.01L4.997,380.677c-6.663,6.664-6.663,17.468,0,24.132
                        c6.664,6.662,17.468,6.662,24.132,0l175.667-175.667l175.667,175.667c6.78,6.548,17.584,6.36,24.132-0.42
                        c6.387-6.614,6.387-17.099,0-23.712L228.929,205.01z"/>
            </svg>
        </div>
        <div class="geo-label-container"> </div>
    </div>
</div>

<div class="modal_wrapper" id="geo-label-no-region">
    <div class="modal_block_region geo-label-modal geo-label-shadow ">
        <div class="btn-close  modal_close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 409.806 409.806" style="enable-background:new 0 0 409.806 409.806;" xml:space="preserve">
		            <path d="M228.929,205.01L404.596,29.343c6.78-6.548,6.968-17.352,0.42-24.132c-6.548-6.78-17.352-6.968-24.132-0.42
                        c-0.142,0.137-0.282,0.277-0.42,0.42L204.796,180.878L29.129,5.21c-6.78-6.548-17.584-6.36-24.132,0.42
                        c-6.388,6.614-6.388,17.099,0,23.713L180.664,205.01L4.997,380.677c-6.663,6.664-6.663,17.468,0,24.132
                        c6.664,6.662,17.468,6.662,24.132,0l175.667-175.667l175.667,175.667c6.78,6.548,17.584,6.36,24.132-0.42
                        c6.387-6.614,6.387-17.099,0-23.712L228.929,205.01z"/>
            </svg>
        </div>
        <div class="geo-label-content">
            <p>В этом регионе у нас пока нет друзей.</p>
            <p>Наши друзья получают множество
                преимуществ.</p>
            <p>
                <b>Присоединяйтесь!</b>
            </p>
            <div class="btn-wrap modal-join-btn-wrap">
                <a href="javascript:void(0)" class="btn-user  btn-get-modal btn-min btn" data-id="#modal-send-form" data-source="Подружиться"><span class="btn-inner">Подружиться</span></a>
            </div>
        </div>
    </div>
</div>



<div class="geo-label-unit-block geo-label-hidden" id="geo-label-unit-block">
    <div class="geo-label-unit geo-label-shadow geo-label-region">

    </div>
</div>


<div class="geo-label-unit-block geo-label-hidden" id="geo-label-unit-block-no-region">
    <div class="geo-label-unit geo-label-shadow geo-no-region" >
        <div class="geo-label-content">
            <p>В этом регионе у нас пока нет друзей. <br>
                Наши друзья получают множество
                преимуществ.
            </p>
            <p>
                <b>Присоединяйтесь!</b>
            </p>
            <div class="btn-wrap">
                <a href="javascript:void(0)" class="btn-user  btn-get-modal btn-tiny btn" data-id="#modal-send-form" data-source="Подружиться"><span class="btn-inner">Подружиться</span></a>
            </div>
        </div>
    </div>
</div>

