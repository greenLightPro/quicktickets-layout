<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<!-- Behavioral Meta Data -->
	
	<meta charset="utf-8">
    <title></title>
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->

	<!-- Core Meta Data -->
	
	<!--<meta name="author" content="Konstantin Voronin">-->
	<meta name="keywords" content="" />
    <meta name="description" content=""/>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!--<meta http-equiv="imagetoolbar" content="no"/>-->
    <meta name="format-detection" content="telephone=no"/>
    <meta name="format-detection" content="address=no"/>

	<!-- Open Graph Meta Data -->
	
	<meta property="og:description" content="Simple, lightweight Parallax Engine that reacts to the orientation of a smart device">
	<meta property="og:image" content="http://wagerfield.github.io/parallax/assets/images/thumbnail.png">
	<meta property="og:site_name" content="parallax.js">
	<meta property="og:title" content="parallax.js">
	<meta property="og:type" content="website">
	<meta property="og:url" content="http://wagerfield.github.io/parallax/index.html">

	<!-- Twitter Card Meta Data -->
	
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="http://wagerfield.github.io/parallax/index.html">
	<meta name="twitter:creator" content="@wagerfield">
	<meta name="twitter:title" content="parallax.js">
	<meta name="twitter:description" content="Simple, lightweight Parallax Engine that reacts to the orientation of a smart device">
	<meta name="twitter:image" content="http://wagerfield.github.io/parallax/assets/images/thumbnail.png">
	
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="css/style.css" type="text/css">
	

</head>
<body>



