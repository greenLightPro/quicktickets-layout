<?php header("Content-Type: text/html; charset=utf-8");?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <?php include_once "parts/header.php";?>
</head>
<body>
<div class="menu-overlay"></div>
<div class="main-wrap">
    <div class="main-body">

        <div class="block-1-wrap">

            <div class="block-1-landscape-img-container">
                <img src="img/sketch_landscape.png" alt="" class="block-1-landscape-img">
            </div>

            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4"></div>
            </div>

            <div class="main-menu-block">
                <div class="container">
                    <div class="main-menu">
                        <div class="btn-menu-close"></div>
                        <ul class="menu-list">
                            <li><a href="#" class="btn-go" data-go="#reviews">Отзывы</a></li>
                            <li><a href="#" class="btn-go" data-go="#support">Поддержка учреждений</a></li>
                            <li><a href="#" class="btn-go" data-go="#marketing">Маркетинг и продажи</a></li>
                            <li><a href="#" class="btn-go" data-go="#results">Результаты</a></li>
                            <li><a href="#" class="btn-go" data-go="#cost">Стоимость</a></li>
                            <li><a href="#" class="btn-go" data-go="#adaptation">Как легко перейти</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <header class="header-block">
                <div class="container">
                    <div class="header">
                        <div class="head-logo">
                            <div class="logo-unit">
                                <a class="logo-icon"><img src="img/logo_fill.png" alt=""></a>
                                <div class="logo-content">
                                    <p class="logo-name"><img src="img/logo_name.png" alt=""></p>
                                    <p>Билетная система для <br> учреждений культуры</p>
                                </div>
                            </div>
                        </div>

                        <div class="head-content">
                            <div class="head-content__test">
                                <div class="head-content__test-desc">
                                    Узнайте, какие показатели
                                    вашего учреждения сможет
                                    улучшить Quick Tickets
                                </div>
                                <div class="head-content__test-btn">
                                    <a href="https://vk.com/app6656524_-165981194" class="btn-user-outline btn-go-test btn" target="_blank" >
                                        <span class="btn-inner">Пройти тест</span>
                                    </a>
                                </div>

                            </div>
                            <div class="head-content_contacts">
                                <div class="phone-and-call-unit">
                                    <p class="phone"><a href="tel:+74852336090">+7 (4852) 33-60-90</a></p>
                                    <p><a href="javascript:void(0)" class="btn-link btn-get-modal" data-id="#modal-send-form" data-source="Обратный звонок">Перезвоните мне</a></p>
                                    <div class="phone-and-call-icon">
                                        <div class="round-outer"></div>
                                        <img src="img/phone_icon.svg" alt="">
                                    </div>
                                </div>

                                <div class="u-soc-list-outer">
                                    <ul class="u-soc-list">
                                        <li><a href="https://vk.com/qtstart" target="_blank" class="u-soc-unit"><img src="img/vk.svg" alt=""></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="btn-menu"><span></span></div>

                    </div>
                </div>
            </header>

            <section class="header-cellar-contacts-block">
                <div class="container">
                    <div class="header-cellar-contacts">
                        <div class="phone-and-call-unit">
                            <p class="phone"><a href="tel:+74852336090">+7 (4852) 33-60-90</a></p>
                            <p><a href="javascript:void(0)" class="btn-link btn-get-modal" data-id="#modal-send-form" data-source="Обратный звонок">Перезвоните мне</a></p>
                        </div>

                        <div class="u-soc-list-outer">
                            <ul class="u-soc-list">
                                <li><a href="https://vk.com/qtstart" target="_blank" class="u-soc-unit"><img src="img/vk.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <section class="header-cellar-block">
                <div class="container">
                    <div class="header-cellar">
                        <div class="header-cellar__test">
                            <div class="header-cellar__test-desc">
                                Узнайте, какие показатели
                                вашего учреждения сможет
                                улучшить Quick&nbsp;Tickets
                            </div>
                            <div class="header-cellar__test-btn">
                                <a href="https://vk.com/app6656524_-165981194" class="btn-user-outline btn btn-go-test" target="_blank">
                                    <span class="btn-inner">Пройти тест</span>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="block-1-block">
                <div class="container">
                    <div class="block-1">

                        <img src="img/ballerina.png" alt="" class="block-1-img wow fadeInRight">

                        <div class="block-1-content  wow fadeInLeft">
                            <h1 class="title __big-text"><b>Почему</b> государственные <br>
                                театры и филармонии <br>
                                переходят на <br>
                                <b class="text-nowrap">Quick Tickets?</b>
                            </h1>
                            <div class="btn-wrap">
                                <a href="javascript:void(0)" class="btn-user btn btn-get-modal" data-id="#modal-send-form" data-source="Почему театры и филармонии переходят на Quick Tickets?">
                                    <span class="btn-inner">Узнать подробнее</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="num-preference-wrap">
                        <div class="num-preference-box">

                            <div class="num-preference-box__item">
                                <div class="num-preference-unit wow fadeInLeft" data-wow-delay="0s">
                                    <div class="num-preference-icon">
                                        <div class="num-preference-icon__outer"></div>
                                        <div class="num-preference-icon__inner">
                                            <b class="num-preference-icon__value">0</b>
                                            <span class="num-preference-icon__unit">руб.</span>
                                        </div>
                                    </div>
                                    <div class="num-preference-text">
                                        <b>за продажи через кассу</b> <br>
                                        и уполномоченными
                                    </div>
                                </div>
                            </div>

                            <div class="num-preference-box__item">
                                <div class="num-preference-unit wow fadeInLeft" data-wow-delay="0.1s">
                                    <div class="num-preference-icon">
                                        <div class="num-preference-icon__outer"></div>
                                        <div class="num-preference-icon__inner">
                                            <b class="num-preference-icon__value">0</b>
                                            <span class="num-preference-icon__unit">руб.</span>
                                        </div>
                                    </div>
                                    <div class="num-preference-text">
                                        <b>абонентская плата</b> <br>
                                        и обслуживание касс <br>
                                        по 54-ФЗ
                                    </div>
                                </div>
                            </div>

                            <div class="num-preference-box__item">
                                <div class="num-preference-unit wow fadeInLeft" data-wow-delay="0.2s">
                                    <div class="num-preference-icon">
                                        <div class="num-preference-icon__outer"></div>
                                        <div class="num-preference-icon__inner">
                                            <b class="num-preference-icon__value">8</b>
                                            <span class="num-preference-icon__unit">лет</span>
                                        </div>
                                    </div>
                                    <div class="num-preference-text">
                                        <b>опыта работы</b> <br>
                                        в сфере культуры
                                    </div>
                                </div>
                            </div>

                            <div class="num-preference-box__item">
                                <div class="num-preference-unit wow fadeInLeft" data-wow-delay="0.3s">
                                    <div class="num-preference-icon">
                                        <div class="num-preference-icon__outer"></div>
                                        <div class="num-preference-icon__inner bright-color">
                                            <b class="num-preference-icon__value">98</b>
                                            <span class="num-preference-icon__unit">%</span>
                                        </div>
                                    </div>
                                    <div class="num-preference-text">
                                        учреждений <b>увеличивают <br>
                                            продажи билетов</b> после <br>
                                        подключения
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </section>
        </div>

        <section class="geography-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3 "></div>
                <div class="grid-block__item __item-4">
                    <div class="grid-cover-element"></div>
                </div>
            </div>
            <div class="container">
                <div class="geography">
                    <div class="title-block">
                        <p class="title">
                            Учреждения культуры <br>
                            <b>из 36 регионов</b> перешли <br>
                            на Quick Tickets
                        </p>
                    </div>

                    <?php include_once "parts/geo-map.php"; ?>
                </div>
            </div>
        </section>


        <section class="reviews-wrap-block" id="reviews">
            <div class="reviews-head-block">
                <div class="grid-block">
                    <div class="grid-block__item __item-1"></div>
                    <div class="grid-block__item __item-2"></div>
                    <div class="grid-block__item __item-3 "></div>
                    <div class="grid-block__item __item-4 grid-line-left-none">
                        <div class="grid-cover-element"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="reviews-head">

                        <div class="title-block wow fadeInLeft">
                            <div class="reviews-title-block__round"></div>
                            <p class="title __big-text"> <b>Какие впечатления</b> у театров <br>
                                и филармоний от работы <br>
                                Quick Tickets
                            </p>
                        </div>

                        <div class="reviews-theater-img-wrap wow fadeIn">
                            <img src="img/theater_building_crop.png" alt="" class="reviews-theater-img">
                        </div>
                    </div>
                </div>
            </div>

            <div class="reviews-body-block">
                <div class="container">
                    <div class="reviews-body reviews-wrapper">
                        <form class="reviews-tab-head">
                            <label class="item wow fadeInUp reviews-tab-radio">
                                <input type="radio" name="reviews-tab" value="theater" checked="checked" autocomplete="off">
                                <span class="btn-user-outline btn-user-outline__white btn  __big-height">
                                    <span class="btn-inner">отзывы театров</span>
                                </span>
                            </label>
                            <label class="item wow fadeInUp reviews-tab-radio">
                                <input type="radio" name="reviews-tab" value="philharmonic" autocomplete="off">
                                <span class="btn-user-outline btn-user-outline__white btn __big-height">
                                    <span class="btn-inner">отзывы филармоний</span>
                                </span>
                            </label>
                        </form>

                        <div class="reviews-sliders-box wow fadeIn">
                            <div class="reviews-slider-select-outer">

                            </div>

                            <div class="reviews-slider-video-outer">

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="trust-contacts-block border-bottom-block">
            <img src="img/sketch_theater.png" alt="" class="trust-contacts-img">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2  grid-line-left-none"></div>
                <div class="grid-block__item __item-3 "></div>
                <div class="grid-block__item __item-4"></div>
            </div>
            <div class="container">
                <div class="trust-contacts">

                    <img src="img/conductor_with_sun.png" alt="" class="conductor-with-sun-img wow fadeInLeft">

                    <div class="trust-contacts-content wow fadeInRight">
                        <p class="big-title __big-text">
                            Хотите <br>
                            убедиться <br>
                            лично?
                        </p>

                        <p class="subtitle">Спросите напрямую <br>
                            учреждения, которые уже <br>
                            перешли на Quick Tickets </p>

                        <div class="btn-box-outer">
                            <div class="btn-box">
                                <div class="item">
                                    <a href="javascript:void(0)" class="btn-user-outline btn-user-outline__orange __big-height btn btn-get-modal" data-id="#modal-send-form" data-source="Получить контакты театров, что уже перешли на Quick Tickets" data-title="<b>Заполните форму</b>, <br> и с разрешения театров мы предоставим Вам их контакты.">
                                        <span class="btn-inner">контакты театров</span>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="javascript:void(0)" class="btn-user-outline btn-user-outline__orange __big-height btn btn-get-modal" data-id="#modal-send-form" data-source="Получить контакты филармоний, что уже перешли на Quick Tickets" data-title="<b>Заполните форму</b>, <br> и с разрешения филармоний мы предоставим Вам их контакты.">
                                        <span class="btn-inner">контакты филармоний</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="ticket-system-problem-block __part-1 border-bottom-block">

            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4"></div>
            </div>

            <div class="container">
                <div class="ticket-system-problem">
                    <div class="title-block wow fadeInLeft">
                        <p class="title __big-text">
                            <b>Какой билетной <br>
                                системой</b> вы пользуетесь <br>
                            сейчас?
                        </p>
                        <p class="subtitle">
                            Впрочем, <b>хотя бы одна проблема <br> из  этого списка вам точно знакома:</b>
                        </p>
                    </div>

                    <div class="ticket-system-problem-box box-4-in-row">
                        <div class="ticket-system-problem-unit">
                            <div class="arrow-icon-unit  wow fadeInLeft" data-wow-delay="0s">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>
                            <div class="text-inner  wow fadeInLeft" data-wow-delay="0s">
                                <b>Функционал
                                    не&nbsp;адаптирован
                                    для учреждения <br> культуры,</b> например,
                                не&nbsp;продумана работа
                                с&nbsp;уполномоченными
                            </div>
                        </div>
                        <div class="ticket-system-problem-unit">
                            <div class="arrow-icon-unit  wow fadeInLeft" data-wow-delay="0.1s">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>
                            <div class="text-inner  wow fadeInLeft" data-wow-delay="0.1s">
                                <b>Приходится долго <br>
                                    ждать нужные <br> отчеты,</b> либо <br> формировать их вручную
                            </div>
                        </div>
                        <div class="ticket-system-problem-unit">
                            <div class="arrow-icon-unit wow fadeInLeft" data-wow-delay="0.2s">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>
                            <div class="text-inner wow fadeInLeft" data-wow-delay="0.2s">
                                <b>Маркетинг либо <br> платный,</b> либо <br>
                                не&nbsp;учитывает особенности работы <br> со зрителем в&nbsp;гос. учреждении
                            </div>
                        </div>
                        <div class="ticket-system-problem-unit">
                            <div class="arrow-icon-unit  wow fadeInLeft" data-wow-delay="0.3s">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>
                            <div class="text-inner wow fadeInLeft" data-wow-delay="0.3s">
                                <b>Тех. поддержка
                                    не&nbsp;может оперативно решить ваши проблемы,</b> либо вообще не&nbsp;понимает что вы хотите
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="ticket-system-problem-block __part-2 border-bottom-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4 grid-line-left-none"></div>
            </div>
            <div class="container">
                <div class="ticket-system-problem">

                    <div class="ticket-system-problem-box box-2-in-row __box-1">
                        <div class="ticket-system-problem-unit bg-fill">
                            <div class="arrow-icon-unit  wow fadeInLeft" data-wow-delay="0s">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>
                            <div class="text-inner wow fadeInLeft" data-wow-delay="0s">
                                <b>Дополнительная <br>
                                    плата за <br> обслуживание</b> <br> онлайн-касс по&nbsp;54-ФЗ
                            </div>
                        </div>
                        <div class="ticket-system-problem-unit wow fadeInLeft">
                            <div class="arrow-icon-unit  wow fadeInLeft"  data-wow-delay="0.1s">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>
                            <div class="text-inner  wow fadeInLeft"  data-wow-delay="0.1s">
                                <b>Никакой информационной
                                    и рекламной поддержки</b> в&nbsp;сторону учреждения
                            </div>
                        </div>
                    </div>

                    <div class="ticket-system-problem-box box-2-in-row __border-top-shift __box-2">
                        <div class="ticket-system-problem-unit">
                            <div class="arrow-icon-unit  wow fadeInLeft" data-wow-delay="0s">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>
                            <div class="text-inner  wow fadeInLeft" data-wow-delay="0s">
                                <b>Задержки
                                    с&nbsp;перечислениями</b>
                            </div>
                        </div>
                        <div class="ticket-system-problem-unit bg-fill">
                            <div class="arrow-icon-unit  wow fadeInLeft" data-wow-delay="0.1s">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>
                            <div class="text-inner  wow fadeInLeft" data-wow-delay="0.1s">
                                <b>Не выходят обновления под ваши запросы</b>
                            </div>
                        </div>
                    </div>

                    <img src="img/actors_action.png" alt="" class="actors-discuss-img wow fadeInRight">
                </div>
            </div>
        </section>

        <section class="why-so-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4"></div>
            </div>
            <div class="container">
                <div class="why-so">
                    <img src="img/question_big_icon.png" alt="" class="why-so-question-icon wow fadeInUp">
                    <div class="title-block wow fadeInLeft">
                        <p class="title">
                            <b>Почему так</b> <br>
                            происходит?
                        </p>
                    </div>

                    <div class="why-so-box">
                        <div class="item __item-1">
                            <div class="why-so-unit wow fadeInLeft">
                                <div class="cover"><img src="img/reason_icon_1.png" alt=""></div>
                                <div class="text">
                                    <p class="u-title">
                                        Билетные сервисы <br>
                                        стремятся охватить <br>
                                        большую аудиторию
                                    </p>
                                    <p>
                                        Большинство систем делаются
                                        универсальными, <b>не&nbsp;учитывая <br>
                                            нужды государственных учреждений.</b> <br>
                                        Их задача - привлечь наибольшее
                                        число организаций.
                                    </p>
                                    <p>
                                        Тех. поддержке некогда заниматься
                                        решением индивидуальных <br>
                                        проблем.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item __item-2">
                            <div class="why-so-unit  wow fadeInLeft">
                                <div class="cover"><img src="img/reason_icon_2.png" alt=""></div>
                                <div class="text">
                                    <p class="u-title">
                                        Главный приоритет&nbsp;— <br>
                                        коммерческие <br>
                                        организации
                                    </p>
                                    <p>
                                        Продвигать дорогое  звёздное шоу со стоимостью билетов от 3000 рублей выгоднее,
                                        чем мероприятия, где билет стоит меньше 1000&nbsp;р.
                                        Такая билетная система основной доход получает
                                        с&nbsp;коммерческих организаций.
                                    </p>
                                    <p>
                                        <b>Зачем ей ставить на первое место
                                            ваше учреждение?</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item __item-3">
                            <div class="why-so-unit  wow fadeInLeft">
                                <div class="cover"><img src="img/reason_icon_3.png" alt=""></div>
                                <div class="text">
                                    <p class="u-title">
                                        Мало кто понимает <br>
                                        специфику учреждений <br>
                                        культуры
                                    </p>
                                    <p>
                                        Операторы <b>не&nbsp;углубляются</b> в то, <br>
                                        как построена работа со зрителем
                                        и&nbsp;система отчетности в гос. организации.
                                        Для этого нужно изучать билетное хозяйство, понимать взаимодействие с уполномоченными,
                                        обучать специалистов тех. поддержки.
                                    </p>
                                    <p>
                                        <b>Для большинства  систем&nbsp;- это <br>
                                            дорого и&nbsp;невыгодно.</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="go-test-block border-top-block border-bottom-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4"></div>
            </div>

            <div class="container">
                <div class="go-test">
                    <img src="img/statue.png" alt="" class="statue-harp-img wow fadeIn">
                    <div class="title-block wow fadeInLeft">
                        <p class="title __big-text">
                            <b>Вы уже знаете сколько времени, сил и зрителей теряете,</b> <br>
                            не&nbsp;имея необходимого функционала и рекламной поддержки?
                        </p>
                    </div>

                    <div class="go-test-line-block border-top-block border-bottom-block border-left-block wow fadeInUp">
                        <div class="content">
                            <p>
                                <b>Пройдите короткий тест</b>, мы проведем анализ <br>
                                вашей системы и покажем как Quick Tickets <br>
                                сможет улучшить ваши показатели
                            </p>
                        </div>
                        <div class="btn-wrap">
                            <a href="https://vk.com/app6656524_-165981194" class="btn-user btn btn-go-test" target="_blank" >
                                <span class="btn-inner">пройти тест</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="unique-system-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4"></div>
            </div>
            <div class="container">
                <div class="unique-system">
                    <div class="title-block">
                        <p class="title __big-text wow fadeInLeft">
                            Государственное учреждение культуры&nbsp;— <br>
                            <b>это отдельный мир со своей уникальной <br>
                                спецификой,</b> абсолютно не&nbsp;похожей <br>
                            на работу коммерческих организаций
                        </p>

                        <div class="quick-tickets-note-unit  wow fadeInLeft">
                            <div class="icon"><img src="img/logo.png" alt=""></div>
                            <div class="content">В Quick Tickets это понимают</div>
                        </div>
                    </div>
                    <div class="unique-system-content-box">
                        <div class="content border-top-block  wow fadeInLeft">
                            <p class="u-title">Мы НЕ&nbsp;создаем универсальную систему и НЕ&nbsp;пытаемся угодить всем</p>
                            <p>
                                Quick Tickets - это уникальное решение, созданное на основании реальных запросов гос. учреждений.
                                Подключая всё больше театров и филармоний, мы постоянно совершенствуем нашу систему, <br>
                                учитывая каждое их пожелание.
                            </p>
                        </div>

                        <div class="side">
                            <div class="big-digit-box  wow fadeInRight">
                                <div class="big-digit-unit">
                                    <div class="big-digit-unit-arc"></div>
                                    <div class="big-digit-unit-inner">
                                        <b>97</b><em>%</em>
                                    </div>
                                </div>
                                <div class="content">
                                    <p>
                                        <b>
                                            всех подключенных <br>
                                            к нам организаций&nbsp;– </b> <br>
                                        это государственные <br>
                                        учреждения <br>
                                        культуры
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="img/eagle_crest.png" alt="" class="eagle-crest-img wow fadeInUp">

                </div>
            </div>
        </section>

        <section class="unique-system-preference-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4"></div>
            </div>
            <div class="container">
                <div class="unique-system-preference">

                    <div class="title-block wow fadeInLeft">
                        <p class="title">
                            Что делает Quick Tickets <b>уникальной <br>
                                билетной системой</b> для государственных <br> учреждений?
                        </p>
                    </div>

                    <div class="unique-preference-box">

                        <div class="unique-preference-unit __unit-1">
                            <div class="cover wow fadeInLeft"><img src="img/qticket_pref_1.png" alt=""></div>
                            <div class="content wow fadeInRight">
                                <div class="u-pref-title-icon">
                                    <div class="icon">
                                        <img src="img/qticket_pref_icon_1.svg" alt="">
                                    </div>
                                    <div class="text">
                                        Высочайший уровень <br>
                                        технической поддержки
                                    </div>
                                </div>

                                <p>
                                    У нас колоссальный опыт обслуживания <br>
                                    театров и филармоний из 36&nbsp;регионов <br>
                                    России.
                                </p>
                                <p>
                                    <b>Моментальная реакция</b> на запросы <br> пользователей и решение проблем <br>
                                    за несколько минут.
                                </p>
                            </div>
                        </div>


                        <div class="unique-preference-unit __unit-2">
                            <div class="cover wow fadeInLeft"><img src="img/qticket_pref_2.png" alt=""></div>
                            <div class="content wow fadeInRight">
                                <div class="u-pref-title-icon">
                                    <div class="icon">
                                        <img src="img/qticket_pref_icon_2.svg" alt="">
                                    </div>
                                    <div class="text">
                                        Функционал, заточенный <br>
                                        под учреждения культуры
                                    </div>
                                </div>

                                <ul>
                                    <li>
                                        <b>Более 70 видов отчётов</b>, соответствующих <br> требованиям проверяющих и контролирующих
                                        <br> органов.
                                    </li>
                                    <li>Удобная и продуманная <b>работа с уполномоченными.</b></li>
                                    <li>Маркетинговый функционал, учитывающий <br> специфику работы со зрителем в гос. учреждении.</li>
                                    <li>Гибкая система <b>пригласительных, льгот <br>
                                            и абонементов.</b></li>
                                    <li>Бесплатное подключение, настройка <br>
                                        и <b>обслуживание онлайн-касс</b> по ФЗ-54</li>
                                </ul>
                            </div>
                        </div>

                        <div class="unique-preference-unit __unit-3">
                            <div class="cover wow fadeInLeft"><img src="img/qticket_pref_3.png" alt=""></div>
                            <div class="content wow fadeInRight">
                                <div class="u-pref-title-icon">
                                    <div class="icon">
                                        <img src="img/qticket_pref_icon_3.svg" alt="">
                                    </div>
                                    <div class="text">
                                        Постоянная реклама вашего учреждения <br>
                                        в поисковых системах и социальных сетях
                                    </div>
                                </div>

                                <p>
                                    Мы тратим ресурсы на расширение вашей зрительской <br> аудитории, так как знаем, что в будущем это окупится
                                    <br>  и принесёт плоды.
                                </p>
                                <p>Привлечённые рекламой <b>зрители попадут в вашу базу,</b>
                                    со временем станут постоянными посетителями <b>и ещё не раз <br> купят билеты.</b></p>
                            </div>
                        </div>

                    </div>


                    <div class="unique-preference-note wow fadeInLeft">
                        <p>
                            Узнайте больше о <b>возможностях Quick Tickets</b>  и пользе для вашей организации, записавшись на бесплатную презентацию в вашем учреждении.
                        </p>

                        <div class="btn-wrap">
                            <a href="javascript:void(0)" class="btn-user btn btn-get-modal" data-id="#modal-send-form"><span class="btn-inner" data-source="Записаться на презентацию">Записаться на презентацию</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="help-support-block border-top-block" id="support">

            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4 grid-line-left-none"></div>
            </div>

            <div class="container">
                <div class="help-support">

                    <img src="img/hands_2.jpg" alt="" class="img-hands wow fadeIn">

                    <div class="title-block wow fadeInLeft">
                        <p class="title __big-text">
                            Что вы обретаете <br>
                            при подключении к Quick Tickets <br>
                            <b>Помощь и поддержку:</b>
                        </p>
                    </div>

                    <div class="help-support-content-box">
                        <div class="help-support-content-item-1">
                            <div class="arrow-icon-unit wow fadeInLeft">
                                <div class="arrow-icon-unit-arc"></div>
                                <img src="img/arrow_icon.svg" alt="">
                            </div>

                            <div class="help-support-content-text wow fadeInLeft">
                                <p class="u-title">Представьте:</p>
                                <ul>
                                    <li>
                                        Вы объясняете про отчёт&nbsp;– <br>
                                        <b>вас понимают.</b>
                                    </li>
                                    <li>
                                        Рассказываете про уполномоченных&nbsp;– <br>
                                        <b>вас понимают.</b>
                                    </li>
                                    <li>
                                        Просите дополнительно помочь <br>
                                        с информационной и рекламной <br>
                                        поддержкой – <b>вам не отказывают.</b>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="help-support-content-item-2">
                            <div class="help-support-content-text wow fadeInRight">
                                <p class="u-title">
                                    Команда тех. поддержки Quick <br>
                                    Tickets&nbsp;– это наш фундамент
                                </p>
                                <p>
                                    Они на практике знают, что такое билетное <br>
                                    хозяйство театра, а что – филармонии. <br>
                                    <b>Именно благодаря их профессионализму,</b> <br>
                                    многолетнему опыту и умению помочь в любой <br>
                                    ситуации, <b>сотрудники учреждений культуры,</b> <br>
                                    подключенных к Quick Tickets, <b>так спокойны</b>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="reader-box">
                        <div class="reader-box-item-1">
                            <img src="img/reader.icon.png" alt="" class="reader-img wow fadeInLeft">
                        </div>
                        <div class="reader-box-item-2 wow fadeInRight">
                            <p>
                                Вы действительно <br>
                                чувствуете помощь <br>
                                и поддержку, <br>
                                и самое <br>
                                главное - <b>с вами <br>
                                    наконец-то разговаривают <br>
                                    на одном языке</b>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="marketing-block border-top-block" id="marketing">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4 grid-line-left-none"></div>
            </div>
            <div class="container">
                <div class="marketing">
                    <img src="img/gray_tape.jpg" alt="" class="marketing-tape-img wow fadeIn">
                    <div class="title-block border-bottom-block wow fadeInLeft">
                        <p class="title"><b>Маркетинг</b> <br>
                            для учреждения
                        </p>
                    </div>

                    <div class="marketing-box">
                        <div class="marketing-item marketing-item-left">
                            <div class="marketing-item-unit bg-white">
                                <div class="round-icon wow fadeInLeft" data-wow-delay="0s">
                                    <div class="round-icon-arc"></div>
                                    <img src="img/marketing_icon_1.svg" alt="">
                                </div>
                                <div class="marketing-item-unit-text wow fadeInLeft" data-wow-delay="0s">
                                    <p class="u-title">
                                        Персонализированные <br> e-mail-рассылки
                                    </p>
                                    <p>
                                        Все привлечённые зрители попадают <br>
                                        напрямую в базу вашего учреждения. <br>
                                        Вы можете делать гибкие сегментированные <br>
                                        <b>рассылки по всей своей аудитории.</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="marketing-item marketing-item-right">
                            <div class="marketing-item-unit">
                                <div class="round-icon wow fadeInLeft" data-wow-delay="0.1s">
                                    <div class="round-icon-arc"></div>
                                    <img src="img/marketing_icon_2.svg" alt="">
                                </div>
                                <div class="marketing-item-unit-text wow fadeInLeft" data-wow-delay="0.1s">
                                    <p class="u-title">
                                        Гибкие подарочные <br>
                                        сертификаты
                                    </p>
                                    <p>
                                        Реализованы с соблюдением всех <br>
                                        рекомендаций ФНС по реализации и учёту <br>
                                        подарочных сертификатов. <b>Одобрены <br>
                                        бухгалтериями театров и филармоний.</b>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="marketing-item marketing-item-left">
                            <div class="marketing-item-unit bg-fill marketing-item-unit-3">
                                <div class="round-icon wow fadeInLeft" data-wow-delay="0s">
                                    <div class="round-icon-arc"></div>
                                    <img src="img/marketing_icon_3.svg" alt="">
                                </div>
                                <div class="marketing-item-unit-text wow fadeInLeft" data-wow-delay="0s">
                                    <p class="u-title">
                                        Активная реклама вашего <br> учреждения силами Quick <br> Tickets
                                    </p>
                                    <p>
                                        С первых дней. Бесплатно. Мы считаем, <br>
                                        что это честный и правильный подход&nbsp;- <br>
                                        <b>вы не должны заниматься привлечением <br>
                                            зрителей в одиночку.</b>
                                    </p>
                                </div>

                                <img src="img/gift_layer.jpg" alt="" class="bg-gift-layer wow fadeIn">

                            </div>
                        </div>
                        <div class="marketing-item marketing-item-right __column">
                            <div class="marketing-item-unit bg-white">
                                <div class="round-icon wow fadeInLeft" data-wow-delay="0.1s">
                                    <div class="round-icon-arc"></div>
                                    <img src="img/marketing_icon_4.svg" alt="">
                                </div>
                                <div class="marketing-item-unit-text wow fadeInLeft" data-wow-delay="0.1s">
                                    <p class="u-title">
                                        Карты лояльности <br>
                                        зрителя
                                    </p>
                                    <p>
                                        Позволяют выстраивать собственную <br>
                                        уникальную систему лояльности. Такая <br>
                                        <b>система стимулирует зрителя регулярно <br>
                                        посещать Ваше учреждение,</b> получая <br>
                                        за это всё более ценные привилегии
                                    </p>
                                </div>
                            </div>

                            <div class="marketing-item-unit bg-white">
                                <div class="round-icon wow fadeInLeft" data-wow-delay="0.2s">
                                    <div class="round-icon-arc"></div>
                                    <img src="img/marketing_icon_5.svg" alt="">
                                </div>
                                <div class="marketing-item-unit-text wow fadeInLeft" data-wow-delay="0.2s">
                                    <p class="u-title">
                                        Система пригласительных, <br>
                                        скидок и льготных <br>
                                        категорий граждан
                                    </p>
                                    <p>
                                        Как и другие модули, реализованы
                                        по просьбе гос. учреждений.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>

        <section class="package-service-block border-top-block">

            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"><div class="__grid-cover-bottom"></div></div>
                <div class="grid-block__item __item-4"><div class="__grid-cover-bottom"></div></div>
            </div>

            <div class="container">
                <div class="package-service">

                    <div class="title-block wow fadeInLeft">
                        <p class="title"><b>Комплексное</b> <br> обслуживание</p>
                    </div>

                    <div class="package-service-outer-box">

                        <div class="package-service-box">
                            <div class="package-service-item __item-half">
                                <div class="arrow-icon-unit wow fadeInLeft" data-wow-delay="0s">
                                    <div class="arrow-icon-unit-arc"></div>
                                    <img src="img/arrow_icon.svg" alt="">
                                </div>
                                <div class="package-service-text wow fadeInLeft" data-wow-delay="0s">
                                    <b>Настройка, <br>
                                        регистрация, <br>
                                        и полная интеграция</b> <br>
                                        онлайн-касс <br>
                                        с билетной системой
                                </div>
                            </div>

                            <div class="package-service-item __item-half __bg-fill __bg-with-cover-border">
                                <div class="arrow-icon-unit wow fadeInLeft" data-wow-delay="0.1s">
                                    <div class="arrow-icon-unit-arc"></div>
                                    <img src="img/arrow_icon.svg" alt="">
                                </div>
                                <div class="package-service-text wow fadeInLeft" data-wow-delay="0.1s">
                                    <b>Их дальнейшее бесплатное обслуживание</b>
                                </div>
                            </div>

                            <div class="package-service-item __item-full __bg-white">
                                <div class="arrow-icon-unit wow fadeInLeft" data-wow-delay="0.1s">
                                    <div class="arrow-icon-unit-arc"></div>
                                    <img src="img/arrow_icon.svg" alt="">
                                </div>
                                <div class="package-service-text wow fadeInLeft" data-wow-delay="0.1s">
                                    <b>Помощь с обслуживанием термопринтеров, <br>
                                        поставка термобилетов, онлайн-касс и даже <br>
                                        фискальных накопителей</b>
                                </div>
                            </div>
                        </div>

                        <div class="package-service-video-box">

                            <div class="package-service-video wow fadeInUp u-video-block">
                                <div class="round-decor"></div>
                                <div class="package-service-video-cover u-video-cover">
                                    <img src="https://i.ytimg.com/vi/_R3uEJWj7h0/0.jpg" alt="">
                                    <a href="javascript:void(0)" class="btn-play u-video-play"><img src="img/play2.svg" alt=""></a>
                                </div>
                                <div class="u-video-iframe">
                                    <iframe src="https://www.youtube.com/embed/_R3uEJWj7h0?enablejsapi=1;modestbranding=1;rel=0;controls=1;" width="560" height="315" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <p class="package-service-video-desc wow fadeInUp">Посмотрите наше видео о внедрении в учреждениях культуры онлайн-касс по ФЗ-54</p>

                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="unique-bonus-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4"></div>
            </div>
            <div class="container">
                <div class="unique-bonus">
                    <div class="unique-status-bonus-img-outer">
                        <img src="img/statue_2.jpg" alt="" class="unique-status-bonus-img wow fadeIn">
                    </div>

                    <div class="title-block wow fadeInLeft">
                        <p class="title"><b>Уникальный бонус</b></p>
                        <p class="subtitle">Индивидуальная доработка <br>
                            по вашему желанию</p>
                    </div>

                    <div class="unique-bonus-box">
                        <div class="unique-bonus-unit">
                            <div class="num-icon wow fadeInLeft" data-wow-delay="0s">
                                <div class="num-icon-arc"></div>
                                <div class="num-icon-text">01</div>
                            </div>
                            <div class="unique-bonus-unit-text wow fadeInLeft" data-wow-delay="0s">
                                <p>
                                    <b>Свой штат <br>
                                    разработчиков</b> <br>
                                    позволяет делать <br>
                                    систему такой, какой <br>
                                    вы хотите ее видеть
                                </p>
                            </div>
                        </div>

                        <div class="unique-bonus-unit">
                            <div class="num-icon wow fadeInLeft" data-wow-delay="0.1s">
                                <div class="num-icon-arc"></div>
                                <div class="num-icon-text">02</div>
                            </div>
                            <div class="unique-bonus-unit-text wow fadeInLeft" data-wow-delay="0.1s">
                                <p>
                                    Мы понимаем, <br>
                                    что каждое <b>учреждение <br>
                                    уникально и требует <br>
                                    индивидуального <br>
                                    подхода</b>
                                </p>
                            </div>
                        </div>

                        <div class="unique-bonus-unit">
                            <div class="num-icon wow fadeInLeft" data-wow-delay="0.2s">
                                <div class="num-icon-arc"></div>
                                <div class="num-icon-text">03</div>
                            </div>
                            <div class="unique-bonus-unit-text wow fadeInLeft" data-wow-delay="0.2s">
                                <p>
                                    <b>За 8 лет не взяли <br>
                                    ни копейки ни <br>
                                    с одного учреждения</b> <br>
                                    за реализованные <br>
                                    доработки, о которых <br>
                                    нас просили
                                </p>
                            </div>
                        </div>

                        <div class="unique-bonus-unit">
                            <div class="num-icon wow fadeInLeft" data-wow-delay="0.3s">
                                <div class="num-icon-arc"></div>
                                <div class="num-icon-text">04</div>
                            </div>
                            <div class="unique-bonus-unit-text wow fadeInLeft" data-wow-delay="0.3s">
                                <p>
                                    <b>Мы будем рады <br>
                                        совершенствовать <br>
                                        систему и по вашим <br>
                                        пожеланиям</b>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="result-facts-block border-top-block" id="results">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3"></div>
                <div class="grid-block__item __item-4"></div>
            </div>
            <div class="container">
                <div class="result-facts">
                    <div class="title-block-outer">
                        <div class="title-block wow fadeInLeft">
                            <p class="title __big-text">
                                Результаты перехода на Quick Tickets. <br>
                                <b>Только факты</b>
                            </p>
                        </div>
                    </div>

                    <img src="img/sketch_architecture.jpg" alt="" class="result-facts-sketch  wow fadeIn">

                    <div class="result-facts-outer border-top-block __box-1">
                        <div class="cover  wow fadeIn"><img src="img/statue_3.jpg" alt=""></div>
                        <div class="result-facts-box-content">
                            <p class="u-title  wow fadeInUp">
                               <b>У Новокузнецкого драматического театра</b> <br>
                                после перехода с другой билетной системы <br>
                                на Quick Tickets:
                            </p>

                            <div class="result-facts-box-wrap">
                                <div class="result-facts-box wow fadeInUp">
                                    <div class="item">
                                        <div class="result-facts-unit">
                                            <p>онлайн-продажи <br>
                                                увеличились <br>
                                                в 1,5 раза </p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="result-facts-unit">
                                            <p>внедрение карт лояльности зрителей позволило активно взаимодействовать даже
                                                с теми, кто привык покупать билеты только в кассе </p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="grid-divider"></div>
                    <div class="result-facts-outer __box-2">
                        <div class="cover  wow fadeIn"><img src="img/philharmonic_photo.jpg" alt=""></div>
                        <div class="result-facts-box-content">
                            <p class="u-title wow fadeInUp">
                                <b>В Белгородской государственной <br>
                                филармонии</b> после перехода на Quick Tickets <br>
                                и внедрения карт лояльности слушателей:
                            </p>

                            <div class="result-facts-box-wrap">
                                <div class="result-facts-box wow fadeInUp">
                                    <div class="item">
                                        <div class="result-facts-unit">
                                            <p>увеличились <br>
                                                продажи в кассе </p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="result-facts-unit">
                                            <p>была успешно запущена <br>
                                                онлайн-продажа <br>
                                                абонементов</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="result-facts-unit">
                                            <p>на 40&nbsp;% повысились <br>
                                                онлайн-продажи</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="result-facts-desc-block wow fadeInLeft">
                        <p class="u-title">
                            Узнать подробнее, за счёт чего <b>учреждения <br>
                            начинают увеличивать свою посещаемость,</b> <br>
                            можно пройдя интересный тест для сотрудников <br>
                            театров и филармоний
                        </p>

                        <div class="btn-wrap">
                            <a class="btn-user btn btn-go-test" href="https://vk.com/app6656524_-165981194" target="_blank">
                                <span class="btn-inner">пройти тест</span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="clients-friends-block border-top-block border-bottom-block">
            <div class="clients-friends-head-block">
                <div class="grid-block">
                    <div class="grid-block__item __item-1"></div>
                    <div class="grid-block__item __item-2"></div>
                    <div class="grid-block__item __item-3 grid-line-left-none"></div>
                    <div class="grid-block__item __item-4 grid-line-left-none"></div>
                </div>
                <div class="container">
                    <div class="clients-friends-head">
                        <div class="title-block wow fadeInLeft">
                            <p class="title __big-text">
                                У нас нет клиентов. <b>У&nbsp;нас есть друзья</b> <br>
                                среди организаций культуры
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clients-friends-n-block clients-friends-1-block border-top-block">
                <div class="grid-block">
                    <div class="grid-block__item __item-1"></div>
                    <div class="grid-block__item __item-2"></div>
                    <div class="grid-block__item __item-3 grid-line-left-none"></div>
                    <div class="grid-block__item __item-4"></div>
                </div>
                <div class="container">
                    <div class="clients-friends-1 clients-friends-n">
                        <div class="title-block-icon wow fadeInUp">
                            <div class="icon"><img src="img/theater_icon.png" alt=""></div>
                            <p class="u-title">ТЕАТРЫ</p>
                        </div>

                        <div class="clients-slider-wrap slider-wrap slick-slider-wrap wow fadeIn">
                            <div class="clients-slider slick-self-slider">
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/sevastopolskij_russkij_dramaticheskij_teatr_im_a_v_lunacharskogo.png" alt=""></div>
                                        <div class="text">Севастопольский русский драматический театр им. А. В. Луначарского</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/magadanskij_oblastnoj_teatr_kukol.jpg" alt=""></div>
                                        <div class="text">Магаданский областной театр кукол</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/omskij_dramaticheskij_teatr_galerka.png" alt=""></div>
                                        <div class="text">Омский драматический театр «Галерка»</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/volgogradskij_teatr_yunogo_zritelya.png" alt=""></div>
                                        <div class="text">Волгоградский театр юного зрителя</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/syzranskij_dramaticheskij_teatr_im_a_n_tolstogo.png" alt=""></div>
                                        <div class="text">Сызранский драматический театр им. А. Н. Толстого</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/tulskij_tyuz.png" alt=""></div>
                                        <div class="text">Тульский ТЮЗ</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/vologodskij_tyuz.png" alt=""></div>
                                        <div class="text">Вологодский ТЮЗ</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/vorkutinskij_dramaticheskij_teatr.png" alt=""></div>
                                        <div class="text">Воркутинский драматический театр</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/yaroslavskij_gosudarstvennyj_teatr_kukol.png" alt=""></div>
                                        <div class="text">Ярославский государственный театр кукол</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/rybinskij_teatr_kukol.png" alt=""></div>
                                        <div class="text">Рыбинский театр кукол</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/orlovskij_teatr_svobodnoe_prostranstvo.png" alt=""></div>
                                        <div class="text">Орловский театр «Свободное пространство»</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/novokuzneckij_dramaticheskij_teatr.png" alt=""></div>
                                        <div class="text">Новокузнецкий драматический театр</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/murmanskij_oblastnoj_dramaticheskij_teatr.png" alt=""></div>
                                        <div class="text">Мурманский областной драматический театр</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/kurganskij_teatr_kukol_gulliver.png" alt=""></div>
                                        <div class="text">Курганский театр кукол «Гулливер»</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/kostromskoj_teatr_kukol.png" alt=""></div>
                                        <div class="text">Костромской театр кукол</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/kostromskoj_dramaticheskij_teatr_ostrovskogo.png" alt=""></div>
                                        <div class="text">Костромской драматический театр Островского</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/kamchatskij_teatr_dramy_i_komedii.png" alt=""></div>
                                        <div class="text">Камчатский театр драмы и комедии</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/kaluzhskij_dramaticheskij_teatr.png" alt=""></div>
                                        <div class="text">Калужский драматический театр</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/bryanskij_oblastnoj_teatr_dramy_imeni_a_k_tolstogo.png" alt=""></div>
                                        <div class="text">Брянский областной театр драмы имени А. К. Толстого</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/arhangelskij_teatr_dramy_im_m_v_lomonosova.png" alt=""></div>
                                        <div class="text">Архангельский театр драмы им. М. В. Ломоносова</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/amurskij_teatr_kukol.png" alt=""></div>
                                        <div class="text">Амурский театр кукол</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/akademicheskij_teatr_dramy_im_v_savina.png" alt=""></div>
                                        <div class="text">Академический театр драмы им. В. Савина</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg" data-lazy="img/clients/theater/kaluzhskij_teatr_kukol.png" alt=""></div>
                                        <div class="text">Калужский театр кукол</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/nacionalnyj_teatr_udmurtii.png" alt=""></div>
                                        <div class="text">Национальный театр Удмуртии</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/rybinskij_dramaticheskij_teatr.png" alt=""></div>
                                        <div class="text">Рыбинский драматический театр</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/muzykalnyj_teatr_respubliki_krym _2.png" alt=""></div>
                                        <div class="text">Музыкальный театр Республики Крым</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/starooskolskij_teatr_dlya_detej_i_molodyozhi_im_b_i_ravenskih.png" alt=""></div>
                                        <div class="text">Старооскольский театр для детей и молодёжи им. Б. И. Равенских</div>
                                    </div>
                                </div>
<!--                                <div class="item">-->
<!--                                    <div class="clients-unit">-->
<!--                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/russkij_dramaticheskij_teatr_izhevsk.png" alt=""></div>-->
<!--                                        <div class="text">Русский драматический театр г. Ижевск</div>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/kostromskoj_oblastnoj_teatr_kukol.png" alt=""></div>
                                        <div class="text">Костромской областной театр кукол</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/theater/teatr_opery_i_baleta_g_syktyvkar.png" alt=""></div>
                                        <div class="text">Театр оперы и балета г. Сыктывкар</div>
                                    </div>
                                </div>
                            </div>

                            <div class="btn-slider-container btn-slider-container-prev">
                                <div class="btn-slider btn-slider-prev"></div>
                            </div>
                            <div class="btn-slider-container btn-slider-container-next">
                                <div class="btn-slider btn-slider-next"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="clients-friends-n-block clients-friends-2-block border-top-block">
                <div class="grid-block">
                    <div class="grid-block__item __item-1"></div>
                    <div class="grid-block__item __item-2"></div>
                    <div class="grid-block__item __item-3 grid-line-left-none"></div>
                    <div class="grid-block__item __item-4"></div>
                </div>
                <div class="container">
                    <div class="clients-friends-2 clients-friends-n">
                        <div class="title-block-icon wow fadeInUp">
                            <div class="icon"><img src="img/philharmonic_icon.png"  alt=""></div>
                            <p class="u-title">ФИЛАРМОНИИ</p>
                        </div>

                        <div class="clients-slider-wrap slider-wrap slick-slider-wrap wow fadeIn">
                            <div class="clients-slider slick-self-slider">
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/belgorodskaya_gosudarstvennaya_filarmoniya.png" alt=""></div>
                                        <div class="text">Белгородская государственная филармония</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/pomorskaya_filarmoniya_arhangelsk.png" alt=""></div>
                                        <div class="text">Поморская филармония г.&nbsp;Архангельск</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/amurskaya_oblastnaya_filarmoniya.png" alt=""></div>
                                        <div class="text">Амурская областная филармония</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/tverskaya_oblastnaya_filarmoniya.png" alt=""></div>
                                        <div class="text">Тверская областная филармония</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/komi_respublikanskaya_filarmoniya.png" alt=""></div>
                                        <div class="text">Коми Республиканская филармония</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/bryanskaya_filarmoniya.png" alt=""></div>
                                        <div class="text">Брянская филармония</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/gosudarstvennaya_filarmoniya_kostromskoj_oblasti_2.png" alt=""></div>
                                        <div class="text">Государственная филармония Костромской области</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/leninskij_memorial.jpg" alt=""></div>
                                        <div class="text">Ленинский мемориал</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="clients-unit">
                                        <div class="cover"><img src="img/empty.svg"  data-lazy="img/clients/philharmonic/tulskaya_oblastnaya_filarmoniya.png" alt=""></div>
                                        <div class="text">Тульская областная филармония</div>
                                    </div>
                                </div>
                            </div>

                            <div class="btn-slider-container btn-slider-container-prev">
                                <div class="btn-slider btn-slider-prev"></div>
                            </div>
                            <div class="btn-slider-container btn-slider-container-next">
                                <div class="btn-slider btn-slider-next"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="team-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3 grid-line-left-none"></div>
                <div class="grid-block__item __item-4"></div>
            </div>
            <div class="container">
                <div class="team">
                    <div class="title-block wow fadeInLeft">
                        <p class="title __big-text">
                            <b>Нам действительно</b> удается стать <br>
                            настоящими друзьями  с театрами <br>
                            и филармониями
                        </p>
                        <p class="u-subtitle">
                            Благодаря стараниям той части нашей команды, <br>
                            которая непосредственно взаимодействует <br>
                            с учреждениями
                        </p>
                    </div>

                    <div class="team-slider-wrap slider-wrap border-top-block slick-slider-wrap wow fadeIn">
                        <div class="team-slider slick-self-slider">

                            <div class="team-unit active">
                                <div class="team-unit-cover">
                                    <img src="img/empty.svg" data-lazy="img/team/andrej.jpg" alt="">
                                </div>

                                <div class="team-unit-btn-open"></div>
                                <div class="team-unit-content scrollbar-wrap  scrollbar-inner">
                                    <div class="team-btn-close team-unit-close"></div>
                                    <div class="team-unit-content-inner">
                                        <p class="name">Андрей Куликов</p>
                                        <p class="speciality">Руководитель проекта</p>
                                        <p class="desc">
                                            Работает в компании
                                            с самого основания&nbsp;-
                                            2012 года
                                        </p>
                                        <div class="unit-article">
                                            Я контролирую каждый шаг
                                            по переходу, внедрению
                                            и обучению вашего
                                            учреждения. После&nbsp;– слежу
                                            за тем, чтобы Вам было комфортно, и все Ваши пожелания были услышаны.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="team-unit">
                                <div class="team-unit-cover">
                                    <img src="img/empty.svg" data-lazy="img/team/masha.jpg" alt="">
                                </div>
                                <div class="team-unit-btn-open"></div>
                                <div class="team-unit-content scrollbar-wrap  scrollbar-inner">
                                    <div class="team-btn-close team-unit-close"></div>
                                    <div class="team-unit-content-inner">
                                        <p class="name">Мария</p>
                                        <p class="speciality">Специалист по привлечению зрителей в ваше учреждение</p>
                                        <div class="unit-article">
                                            Делает так, чтобы зрители, ещё незнающие - да узнали о вас. Уже знающие – да купили билет :)
                                            Рисует красивые картины и любит вкусный кофе.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="team-unit">
                                <div class="team-unit-cover">
                                    <img src="img/empty.svg" data-lazy="img/team/maksim.jpg" alt="">
                                </div>
                                <div class="team-unit-btn-open"></div>
                                <div class="team-unit-content scrollbar-wrap  scrollbar-inner">
                                    <div class="team-btn-close team-unit-close"></div>
                                    <div class="team-unit-content-inner">
                                        <p class="name">Максим</p>
                                        <p class="speciality">Руководитель тех. поддержки и отдела внедрения</p>
                                        <div class="unit-article">
                                            Под его чутким руководством сотрудники тех. поддержки работают, как слаженный механизм, готовый оперативно прийти на помощь.
                                            Как считают некоторые кассиры, спокойствию Максима могут позавидовать даже буддийские монахи :)
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--
                            <div class="team-unit">
                                <div class="team-unit-cover">
                                    <img src="img/empty.svg" data-lazy="img/team/ivan.jpg" alt="">
                                </div>
                                <div class="team-unit-btn-open"></div>
                                <div class="team-unit-content scrollbar-wrap  scrollbar-inner">
                                 <div class="team-btn-close team-unit-close"></div>
                                    <div class="team-unit-content-inner">
                                        <p class="name">Иван</p>
                                        <p class="speciality">Главный разработчик и всея программистов</p>
                                        <div class="unit-article">
                                            Именно его пытливый ум выстраивает взаимосвязи, делая Quick Tickets таким удобным.
                                            Его глаза загораются, когда он слышит от вас любую, даже самую маленькую идею по улучшению и совершенствованию системы.
                                            Добавить новый отчёт или разработать по вашей просьбе новую функцию, также естественно для него,
                                            как украсить свой дом или порадовать друзей подарком.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            -->

                            <div class="team-unit">
                                <div class="team-unit-cover">
                                    <img src="img/empty.svg" data-lazy="img/team/olya.jpg" alt="">
                                </div>
                                <div class="team-unit-btn-open"></div>
                                <div class="team-unit-content scrollbar-wrap  scrollbar-inner">
                                    <div class="team-btn-close team-unit-close"></div>
                                    <div class="team-unit-content-inner">
                                        <p class="name">Ольга</p>
                                        <p class="speciality"> Ведущий менеджер Quick Tickets</p>
                                        <div class="unit-article">
                                            Следит за тем, чтобы всё было вовремя и в срок.
                                            Отчёты, перечисления, поставка оборудования&nbsp;– это всё её стихия.
                                            Старается беречь деревья, приучая друзей к электронному документообороту :)
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="btn-slider btn-slider-prev"></div>
                        <div class="btn-slider btn-slider-next"></div>

                    </div>
                </div>
            </div>
        </section>

        <section class="conditions-block border-top-block" id="cost">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3 "></div>
                <div class="grid-block__item __item-4 grid-line-left-none"></div>
            </div>
            <div class="container">
                <div class="conditions">
                    <div class="conditions-wrap">
                        <div class="conditions-content">
                            <div class="title-block wow fadeInLeft">
                                <p class="title"><b>Стоимость</b> и условия</p>
                            </div>
                            <div class="conditions-text-content">
                                <div class="conditions-text-content-inner wow fadeInLeft">
                                    <p>
                                        За 8 лет работы <b>мы добились высочайшего <br>
                                            уровня сервиса,</b> заслужив доверие театров <br>
                                        и филармоний по всей России.
                                    </p>
                                    <p>
                                        В нас велико желание сохранить этот уровень <br>
                                        на долгие годы вперёд, поэтому условия очень <br>
                                        прозрачны:
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="conditions-term">
                            <div class="term-block wow fadeInUp">
                                <div class="term-year">8</div>
                                <div class="term-text">лет работы</div>
                            </div>
                        </div>
                    </div>

                    <div class="conditions-box">
                        <div class="conditions-unit bg-fill">
                            <div class="round-icon wow fadeInLeft" data-wow-delay="0s">
                                <div class="round-icon-arc"></div>
                                <img src="img/conditions_icon_1.svg" alt="">
                            </div>
                            <div class="conditions-unit-text wow fadeInLeft" data-wow-delay="0s">
                                <p>
                                    <b>никаких процентов <br>
                                    за билеты,</b> <br>
                                    проданные в кассе <br>
                                    и уполномоченными
                                </p>
                            </div>
                        </div>
                        <div class="conditions-unit">
                            <div class="round-icon wow fadeInLeft" data-wow-delay="0.1s">
                                <div class="round-icon-arc"></div>
                                <img src="img/conditions_icon_2.svg" alt="">
                            </div>
                            <div class="conditions-unit-text wow fadeInLeft" data-wow-delay="0.1s">
                                <p>
                                    <b>никакой абонентской <br>
                                    платы</b> и денег <br>
                                    за обслуживание
                                </p>
                            </div>
                        </div>
                        <div class="conditions-unit">
                            <div class="round-icon wow fadeInLeft" data-wow-delay="0.2s">
                                <div class="round-icon-arc"></div>
                                <img src="img/conditions_icon_3.svg" alt="">
                            </div>
                            <div class="conditions-unit-text wow fadeInLeft" data-wow-delay="0.2s">
                                <p>
                                    <b>бесплатное внедрение <br>
                                    и обучение</b> ваших <br>
                                    сотрудников
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="your-change-block border-top-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3 "></div>
                <div class="grid-block__item __item-4 grid-line-left-none"></div>
            </div>
            <div class="container">
                <div class="your-change">
                    <div class="your-change-box">
                        <div class="your-change-side your-change-side-1 wow fadeInLeft">
                            <p class="u-title">Вы платите только % с продажи <br>
                                билетов онлайн</p>

                            <div class="your-change-wrap">
                                <div class="your-change-item">
                                    <p>
                                        <b>Размер оплаты&nbsp;</b>- от 3% и зависит от выбранного <br>
                                        варианта сотрудничества.
                                    </p>
                                </div>
                                <div class="your-change-item">
                                    <p><b>Вариант&nbsp;1&nbsp;</b>– вы берёте проценты за онлайн-продажи <br>
                                        полностью на себя.</p>
                                </div>
                                <div class="your-change-item">
                                    <p><b>Вариант&nbsp;2&nbsp;</b>– часть процентов за оформление <br>
                                        электронного билета оплачивает зритель.</div></p>
                            </div>
                        </div>
                        <div class="your-change-side your-change-side-2 wow fadeInRight">
                            <p class="u-title"><b>Какой вариант <br> предпочтительнее?</b></p>
                            <div class="text">
                                <p>Оставьте заявку и наши специалисты <br>
                                    помогут выбрать именно те условия, <br>
                                    которые больше подходят Вашему <br>
                                    учреждению.</p>
                            </div>
                            <div class="btn-wrap"><a href="javascript:void(0)" class="btn-user btn btn-get-modal" data-id="#modal-send-form" data-source="Выбрать условия использования Quick Tickets"><span class="btn-inner">Подобрать вариант</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="adaptation-block" id="adaptation">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3 "></div>
                <div class="grid-block__item __item-4 grid-line-left-none"></div>
            </div>
            <div class="container">
                <div class="adaptation">
                    <div class="title-block adaptation-text-element wow fadeInLeft">
                        <p class="title __big-text">
                           <b>Мы обеспечим легкий переход</b><br>
                            и обучим сотрудников
                        </p>
                    </div>
                    <div class="adaptation-text-1 adaptation-text-element wow fadeInLeft">
                        <p>
                            Чтобы переход был максимально комфортным <br>
                            и безболезненным, <b>мы стараемся приезжать <br>
                            в учреждение, обучая ваших сотрудников <br>
                            на месте.</b>
                        </p>
                    </div>
                    <div class="adaptation-text-2 adaptation-text-element wow fadeInLeft">
                        <p>
                            <b>Работа кассы не останавливается,</b> даже если <br>
                            нашим специалистам будет необходимо переносить <br>
                            продажи билетов из вашей текущей системы <br>
                            в&nbsp;Quick&nbsp;Tickets.
                        </p>
                        <p>
                            Да, мы сами переносим билеты в случае <br> необходимости, а не просим это делать <br>
                            ваших сотрудников.
                        </p>
                    </div>
                    <img src="img/empoyees_photo.jpg" alt="" class="img-staffs-company wow fadeIn">
                </div>
            </div>
        </section>

        <section class="free-presentation-block border-top-block">
            <div class="grid-block">
                <div class="grid-block__item __item-1"></div>
                <div class="grid-block__item __item-2"></div>
                <div class="grid-block__item __item-3 "></div>
                <div class="grid-block__item __item-4 grid-line-left-none"></div>
            </div>
            <div class="container">
                <div class="free-presentation wow fadeInLeft">
                    <p class="u-title">Запишитесь на <b>бесплатную <br>
                        презентацию Quick&nbsp;Tickets</b> <br>
                        в  вашем учреждении</p>

                    <div class="free-presentation-text">
                        <p>Вы познакомитесь с нашей командой лично, а мы
                            продемонстрируем возможности нашей системы
                            и&nbsp;расскажем о ее пользе для вашего учреждения</p>
                    </div>

                    <div class="btn-wrap"><a href="javascript:void(0)" class="btn-user  btn btn-get-modal" data-id="#modal-send-form" data-source="Записаться на презентацию"><span class="btn-inner">Записаться на презентацию</span></a></div>
                </div>
            </div>
        </section>

        <footer class="footer-block border-top-block border-bottom-block">
            <div class="container">
                <div class="footer">
                    <div class="foot-logo">
                        <div class="logo-unit">
                            <a class="logo-icon"><img src="img/logo.png" alt=""></a>
                            <div class="logo-content">
                                <p class="logo-name"><img src="img/logo_name.png" alt=""></p>
                                <p>Билетная система для <br> учреждений культуры</p>
                            </div>
                        </div>
                    </div>
                    <div class="foot-links">
                        <p><a href="docs/quick_tickets_pravovaya_informaciya.pdf" target="_blank">Правовая информация</a></p>
                    </div>
                    <div class="foot-socials">
                        <div class="u-soc-list-outer">
                            <ul class="u-soc-list">
                                <li><a href="https://vk.com/qtstart" target="_blank" class="u-soc-unit"><img src="img/vk.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="foot-contacts">
                        <p class="phone"><a href="tel:+74852336090">+7 (4852) 33-60-90</a></p>
                        <p><a href="javascript:void(0)" class="btn-link btn-get-modal" data-id="#modal-send-form" data-source="Обратный звонок">Перезвоните мне</a></p>
                    </div>
                </div>
            </div>
        </footer>
        
        <div class="cellar-block">
            <div class="container"></div>
            <div class="cellar">

                <p class="cellar-policy"><a href="docs/quick_tickets_pravovaya_informaciya.pdf" target="_blank">Правовая информация</a></p>
                <p class="cellar-data">© 2011 — 2020 Quick Tickets</p>
            </div>
        </div>

    </div>
</div>

<?php include "parts/modals.php";?>
<?php include "parts/footer.php";?>

</body>
</html>