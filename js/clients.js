"use strict";

(function () {
    var clientsSlider = $('.clients-slider');
    if(clientsSlider.length) {
        clientsSlider.each(function(){
            var $slider, $wrap;
            $slider = $(this);
            $wrap = $slider.closest('.slider-wrap');
            $slider.slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                lazyLoad: 'ondemand',
                prevArrow: $wrap.find('.btn-slider-prev'),
                nextArrow: $wrap.find('.btn-slider-next'),
                responsive: [
                    {
                        breakpoint: 961,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 671,
                        settings: {
                            slidesToShow: 1,
                        }
                    },
                ]
            });
        });
    }
}());














