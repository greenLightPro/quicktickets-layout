    var modal_win_top, modal_overlay;
    modal_overlay = $('#modal_overlay');
    $(document).on('click', '.modal_close', function () {
        var obj = $(this).closest('.modal_wrapper');
        modal_to_close(obj);
    });

    $(document).on('click', '.modal_wrapper, #modal_overlay', function (e) {
        var e_target, $this;
        e_target = $(e.target);
        $this = $(this);


        if (e_target.closest('.modal_block').length === 0) {
            modal_to_close('.modal_wrapper');
        }

    });

    $(document).on('click', '.btn-get-modal', function (e) {
        var $this, data_id, data_source, outer_modal_wrapper;
        e.preventDefault();
        e.stopPropagation();
        $this = $(this);
        outer_modal_wrapper = $this.closest('.modal_wrapper');

        data_id = $this.attr('data-id');
        if ($(data_id).length) {
            if(outer_modal_wrapper.length) {
                modal_to_close(outer_modal_wrapper);
            }

            data_source = $this.attr('data-source');
            if (data_source) {
                if($(data_id).find('form .input-form-source').length) {
                    $(data_id).find('form .input-form-source').val(data_source);
                }
            } else {
                $(data_id).find('form .input-form-source').val('');
            }

            let data_title = `<b>Заполните форму</b> и мы <br>
            перезвоним в первую <br>
            свободную минуту!`;

            if ($this.attr('data-title') && $this.attr('data-title') != '') {
                data_title = $this.attr('data-title');
            }
            if ($(data_id).length && $(data_id).find('.modal-title').length > 0 ) {
                $(data_id).find('.modal-title').html(data_title);
            }

            center_modal(data_id);
        }
    });

    function modal_to_close(obj) {
        $('html, body').removeAttr('style').removeClass('show_modal');
        obj = $(obj);
        obj.each(function(){
            obj.scrollTop(0);
            obj.find('.modal_scroll_content').scrollTop(0);
            obj.removeClass('visible');
        });
        modal_overlay.stop().fadeOut(200);
        $(".modal_wrapper iframe").each(function() {
           $(this)[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*')});
        $('html, body').scrollTop(modal_win_top);
    }
    function center_modal(selector) {
        var obj, body, width_scroll;
        modal_win_top = $(window).scrollTop();
        body = $('body');
        obj = $(selector);
        width_scroll = 0;
        if (obj.length == 0) {
            console.log('объект не найден');
            return 0;
        }
        if (is_scroll()) {
            width_scroll = calc_scroll_width();
        } else {
            width_scroll = 0;
        }
        $('html').css({'padding-right': width_scroll + 'px'});
        var set_amim_time = setTimeout(function () {
            $('html').addClass('show_modal');
            clearTimeout(set_amim_time);
        }, 100);
        $('body').css({'overflow': 'hidden'});
        obj.css({'top': modal_win_top + 'px'}).addClass('visible');
        modal_overlay.fadeIn(200);
    }
    function is_scroll() {
        if ($(document).height() > $(window).height()) {
            return true;
        } else {
            return false
        }
    }
    function calc_scroll_width() {
        var hide_block, width_scroll, css_text;
        hide_block = document.createElement('div');
        css_text = "width:100%!important; height:100px; position:fixed; left:100%; top:100%; overflow:scroll;";
        hide_block.id = "hide_block";
        hide_block.setAttribute('style', css_text);
        document.body.appendChild(hide_block);
        width_scroll = parseFloat((hide_block.offsetWidth) - (hide_block.clientWidth), 10);
        hide_block.parentElement.removeChild(hide_block);
        return width_scroll;
    }
