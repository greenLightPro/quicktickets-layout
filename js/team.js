"use strict";

(function () {
    let clientsSlider = $('.team-slider');
    if(clientsSlider.length) {
        clientsSlider.each(function(){
            var $slider, $wrap;
            $slider = $(this);
            $wrap = $slider.closest('.slider-wrap');
            $slider.slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                lazyLoad: 'ondemand',
                prevArrow: $wrap.find('.btn-slider-prev'),
                nextArrow: $wrap.find('.btn-slider-next'),
                responsive: [
                    {
                        breakpoint: 1281,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 901,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 701,
                        settings: {
                            slidesToShow: 1,
                        }
                    },
                ]
            });
        });
    }

    $(document).on('click', '.team-unit', function (e) {
        if ($(e.target).closest('.team-unit-close').length > 0) {
            return false;
        }
        $(this).closest('.team-slider').find('.team-unit').removeClass('active');
        $(this).addClass('active');
    });
    $(document).on('click', '.team-unit-close', function (e) {
        console.log(1);
        $(this).closest('.team-unit').removeClass('active');
        $(this).closest('.team-unit').find('.team-unit-content').scrollTop(0);
    });

    if (window.matchMedia("(max-width: 700px)").matches && $('.team-unit').length > 0) {
        $('.team-unit').removeClass('active');
    }






}());














