"use strict";

let reviewsTheater = {
    theater3: {
        name: 'Рыбинский драматический театр',
        videoCover: 'img/video_reviews/theater/rybinskij_dramaticheskij_teatr.png',
        videoList : ['SDxfiS_Z03E',]
    },
    theater1: {
        name: 'Калужский театр кукол',
        videoCover: 'img/video_reviews/theater/kaluzhskij_teatr_kukol.png',
        videoList: ['dteXOugPqFk',]
    },
    theater4: {
        name: 'Музыкальный театр Республики Крым',
        videoCover: 'img/video_reviews/theater/muzykalnyj_teatr_respubliki_krym.png',
        videoList: ['3URehXyAkyc',]
    },
    theater7: {
        name: 'Волгоградский театр юного зрителя',
        videoCover: 'img/video_reviews/theater/volgogradskij_teatr_yunogo_zritelya.png',
        videoList: ['R-991K6-jH4',]
    },
    theater9: {
        name: 'Театр оперы и балета г. Сыктывкар',
        videoCover: 'img/video_reviews/theater/teatr_opery_i_baleta_g_syktyvkar.png',
        imgList: ['img/reviews/teatr_opery_i_baleta_g_syktyvkar.jpg',]
    },
    theater10: {
        name: 'Севастопольский русский драматический театр им. А. В. Луначарского',
        videoCover: 'img/video_reviews/theater/sevastopolskij_russkij_dramaticheskij_teatr_im_a_v_lunacharskogo.png',
        imgList: [
            ['img/reviews/sevastopolskij_russkij_dramaticheskij_teatr_im_a_v_lunacharskogo_1.jpg', 'img/reviews/sevastopolskij_russkij_dramaticheskij_teatr_im_a_v_lunacharskogo_2.jpg']
        ],
    },
    /*    theater6: {
        name: 'Русский драматический театр г. Ижевск',
        videoCover: 'img/video_reviews/theater/russkij_dramaticheskij_teatr_izhevsk.png',
        videoList: ['0WEcW3e3G1I',]
    },*/
    theater8: {
        name: 'Костромской областной театр кукол',
        videoCover: 'img/video_reviews/theater/kostromskoj_oblastnoj_teatr_kukol.png',
        imgList: ['img/reviews/kostromskoj_oblastnoj_teatr_kukol_review_1.jpg',]
    },
    theater2: {
        name: 'Национальный театр Удмуртии',
        videoCover: 'img/video_reviews/theater/nacionalnyj_teatr_udmurtii.png',
        videoList: ['UCm5MXIjAcE',]
    },
    theater5: {
        name: 'Старооскольский театр для детей и молодёжи им. Б. И. Равенских',
        videoCover: 'img/video_reviews/theater/starooskolskij_teatr_dlya_detej_i_molodyozhi_im_b_i_ravenskih.png',
        videoList: ['ZC76P0GvrQ0',]
    },

};

let reviewsPhilharmonic = {
    philharmonic1: {
        name: 'Тульская областная филармония',
        videoCover: 'img/video_reviews/philharmonic/tulskaya_oblastnaya_filarmoniya.png',
        videoList: ['h4yIxoyNtbI',]
    },
    philharmonic2: {
        name: 'Поморская филармония г. Архангельск',
        videoCover: 'img/video_reviews/philharmonic/pomorskaya_filarmoniya_arhangelsk.png',
        videoList: ['LlimyFtGE-I',]
    },
    philharmonic3: {
        name: 'Государственная филармония Костромской области',
        videoCover: 'img/video_reviews/philharmonic/gosudarstvennaya_filarmoniya_kostromskoj_oblasti.png',
        videoList: ['ueb9om3peGc',]
    },
};


function getSliderSelectHtml (data) {
    let sliderHtml = `
    <div class="reviews-slider-select-container">
        <div class="btn-slider btn-slider-prev __vertical"></div>
        <div class="btn-slider btn-slider-next __vertical"></div>
            <div class="reviews-slider-select">
                <div class="slidee reviews-slider-select-slidee">
    `;
    let n = 1;
    for (let key in data) {
        if (n !== 1) {
            sliderHtml += ` <div class="reviews-slider-select-unit" data-key = "${key}" >`;
        } else {
            sliderHtml += ` <div class="reviews-slider-select-unit active" data-key = "${key}" >`;
            n = 0;
        }
    sliderHtml += `
        <div class="text-inner">
            <p>${data[key].name}</p>
        </div>
    </div>
        `;
    }

    sliderHtml += `
            </div>
        </div>
    </div>
    `;

    return sliderHtml;
}


function getSliderVideoHtml(data, dataKey, htmlWrap = true) {
    let videoCover = data.videoCover;
    let videoList = null;
    let imgList = null;



    if (data.hasOwnProperty('videoList')) {
        videoList =  data.videoList;
    }

    if (data.hasOwnProperty('imgList')) {
        imgList =  data.imgList;
    }

    let sliderHtml = '';

    if (htmlWrap === true) {
        sliderHtml += `
    <div class="reviews-slider-video-container-wrap lg-box">
        <div class="reviews-slider-video-container">
            <div class="reviews-slider-video">
                <div class="slidee reviews-slider-video-slidee">
    `;
    }

    if (videoList !== null && videoList instanceof Array) {
        let videoListCount = videoList.length;
        for (let i = 0; i < videoListCount; i++) {
            sliderHtml += `
            <div class="reviews-slider-video-unit u-video-block" data-key = ${dataKey}>
            
                <div class="u-video-cover reviews-slider-video-cover">   
                    <img src="${videoCover}" alt="" class="reviews-slider-video-img">   
                    <a href="javascript:void(0)" class="btn-play u-video-play"><img src="img/play.svg" alt=""></a>      
                </div>
                
                <div class="u-video-iframe reviews-video-iframe">
                    <iframe src="https://www.youtube.com/embed/${videoList[i]}?enablejsapi=1;modestbranding=1;rel=0;controls=1;" width="560" height="315" frameborder="0" allowfullscreen></iframe>
                </div> 
                 <a href="https://www.youtube.com/watch?v=${videoList[i]}" class="lg-item lg-item-hidden"></a>                                        
            </div>
            `;
        }
    }

    if (imgList !== null && imgList instanceof Array) {
        let imgListCount = imgList.length;

        for (let i = 0; i < imgListCount; i++) {

            if (imgList[i] instanceof Array) {
                sliderHtml += `
                        <div class="reviews-slider-video-unit" data-key = ${dataKey}>
                            <div class="reviews-slider-video-cover">   
                                <img src="${videoCover}" alt="" class="reviews-slider-video-img">`;

                for (let k = 0; k < imgList[i].length; k++) {

                    if (k === 0) {
                        sliderHtml += `<a href="${imgList[i][k]}" class="lg-item btn-zoom"><img src="img/zoom-icon.svg" alt=""></a>`;
                    }
                    else {
                        sliderHtml += `<a href="${imgList[i][k]}" class="lg-item lg-item-hidden"></a>`;
                    }
                }

                sliderHtml += `
                            </div>
                        </div>`;
            } else {
                sliderHtml += `
            <div class="reviews-slider-video-unit" data-key = ${dataKey}>
                <div class="reviews-slider-video-cover">   
                    <img src="${videoCover}" alt="" class="reviews-slider-video-img">     
                    <a href="${imgList[i]}" class="lg-item btn-zoom"><img src="img/zoom-icon.svg" alt=""></a>  
                </div>
                                                       
            </div>
            `;
            }


        }
    }

    if (htmlWrap === true) {
        sliderHtml += `                                               
                </div>
            </div>
        </div>
    </div>
    `;
    }

    return sliderHtml;
}

function toWrapSliderVideoHtml(dataHtml) {
    let sliderHtml = '';

    sliderHtml += `
    <div class="reviews-slider-video-container-wrap lg-box">
        <div class="reviews-slider-video-container">
            <div class="reviews-slider-video">
                <div class="slidee reviews-slider-video-slidee">
    `;
    sliderHtml += dataHtml;
    sliderHtml += `                                               
                </div>
            </div>
        </div>
    </div>
    `;
    return sliderHtml;
}

function getSliderData(selectSliderMode) {
        if (selectSliderMode === 'theater') {
            return reviewsTheater;
    }
        return reviewsPhilharmonic;
}

function bindLgVideoSlider ($videoSlider) {
    $videoSlider.lightGallery({
        thumbnail:true,
        animateThumb: false,
        showThumbByDefault: false,
        selector: '*:not(.slick-clone) .lg-item',
        download: false,
        youtubePlayerParams: {
            modestBranding: 1,
            showInfo: 0,
            rel: 0,
            controls: 1
        }
    });
}

function getValueDependHorizontalMode (a, b) {
    if (getSliderHorizontalMode()) {
        return a;
    } else {
        return b;
    }

}

function getSliderHorizontalMode() {
    if (window.matchMedia("(max-width: 1024px)").matches) {
        return true;
    }
    return false;
}

function doReviewsSlider($reviewsWrap, sliderData) {

    let $sliderSelect = null;
    let $sliderVideo = null;

    doSliderSelect();

    /**
     * Создать видео-слайдер со всеми слайдами
     */
    doSliderVideoAll(sliderData);

    /**
     * Останавливаем видео при смене актива видео-слайдера
     * Сопрягаем видео-слайдер с селект-слайдером
     */

    $sliderVideo.sly('on', 'active', function (eventName, itemIndex ){
        stopVideoAll();

        let slideKeyActive = $sliderVideo.find('.reviews-slider-video-unit').eq(itemIndex).attr('data-key');
        let selectSlideActive = $sliderSelect.find('.reviews-slider-select-unit.active').eq(0);
        if (selectSlideActive.attr('data-key') !== slideKeyActive) {
            let selectSlideKeyIndex = $sliderSelect.find('.reviews-slider-select-unit[data-key='+ slideKeyActive +']').index();
            $sliderSelect.sly('activate', selectSlideKeyIndex);
        }
    });

    /**
     *   Сопрягаем селект-слайдер с видео-слайдером
     */

    $sliderSelect.sly('on', 'active', function (eventName, itemIndex ){

        let slideKeyActive = $sliderSelect.find('.reviews-slider-select-unit').eq(itemIndex).attr('data-key');
        let slideVideoActive = $sliderVideo.find('.reviews-slider-video-unit.active').eq(0);
        if (slideVideoActive.attr('data-key') !== slideKeyActive) {
            $sliderVideo.sly('activate',  $sliderVideo.find('.reviews-slider-video-unit[data-key='+ slideKeyActive +']').index());
        }
    });



    function doSliderSelect() {
        let sliderSelectHtml = getSliderSelectHtml(sliderData);
        $reviewsWrap.find('.reviews-slider-select-outer').eq(0).html(sliderSelectHtml);
        // используется глобальная переменная $sliderSelect  функции doReviewsSlider
        $sliderSelect = $reviewsWrap.find('.reviews-slider-select');
        let $sliderSelectWrap = $sliderSelect.parent();

        let typeComp =  $reviewsWrap.find('.reviews-tab-radio input:checked').val();
        let $sliderSelectStartAt = 0;
        if (typeComp === 'theater') {
            $sliderSelectStartAt = 2;
        } else {
            $sliderSelectStartAt = 0;
        }

        $sliderSelect.sly({
            horizontal: getSliderHorizontalMode() ,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: $sliderSelectStartAt,
            scrollBar: $sliderSelectWrap.find('.scrollbar'),
            scrollBy: 1,
            pagesBar: $sliderSelectWrap.find('.pages'),
            activatePageOn: null,
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            prev: $sliderSelectWrap.find('.btn-slider-prev'),
            next: $sliderSelectWrap.find('.btn-slider-next'),
        });

        if (!getSliderHorizontalMode()) {
            $sliderSelect.sly('toStart' ,0);
        }
    }


    function doSliderVideo(dataSelectKey) {
        let sliderVideoHtml = getSliderVideoHtml(sliderData[dataSelectKey], dataSelectKey);
        pasteHtmlRunVideoSlider(sliderVideoHtml);
    }

    function doSliderVideoAll(sliderData) {
        let sliderVideoHtml = '';
        for (let sliderDataKey in sliderData) {
            sliderVideoHtml += getSliderVideoHtml(sliderData[sliderDataKey], sliderDataKey, false);
        }
        sliderVideoHtml = toWrapSliderVideoHtml(sliderVideoHtml);
        pasteHtmlRunVideoSlider(sliderVideoHtml);
    }

    function pasteHtmlRunVideoSlider(sliderVideoHtml) {
        $reviewsWrap.find('.reviews-slider-video-outer').eq(0).html(sliderVideoHtml);
        $sliderVideo = $reviewsWrap.find('.reviews-slider-video');
        let $reviewsVideoWrap = $sliderVideo.parent();
        bindLgVideoSlider($sliderVideo);

        $sliderVideo.sly({
            horizontal: getSliderHorizontalMode() ,
            // itemNav: 'basic',
            itemNav: function () {return getSliderHorizontalMode() ? 'basic' : 'centered'}(),
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt:  getStartSlideIndexVideoSlider(),
            scrollBar: $reviewsVideoWrap.find('.scrollbar'),
            scrollBy: 1,
            pagesBar: $reviewsVideoWrap.find('.pages'),
            activatePageOn: null,
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,
            prev: $reviewsVideoWrap.find('.btn-slider-prev'),
            next: $reviewsVideoWrap.find('.btn-slider-next'),
        });
    }

    function getStartSlideIndexVideoSlider() {
        let activeSelectSlide =  $reviewsWrap.find('.reviews-slider-select-outer .reviews-slider-select-unit.active').eq(0);
        if (activeSelectSlide.length) {
            return activeSelectSlide.index();
        }
        return 0;
    }
    
}


(function() {
    let $reviewsWrapper = $('.reviews-wrapper');
    if ($reviewsWrapper.length) {
        $reviewsWrapper.each(function (listener) {
            let $reviewsWrap = $(this);

            let $reviewsTabHead = $($reviewsWrap.find('.reviews-tab-head').eq(0));
            let $reviewsTabRadioInputActive = $reviewsTabHead.find('.reviews-tab-radio input:checked');
            let selectSliderMode = $reviewsTabRadioInputActive.val();
            let sliderData = getSliderData(selectSliderMode);

            doReviewsSlider($reviewsWrap, sliderData);
            let mql = window.matchMedia('(max-width: 1024px)');
            if (mql.addEventListener !== undefined) {
                mql.addEventListener('change', function (){
                    doReviewsSlider($reviewsWrap, sliderData);
                });
            } else {
                mql.addListener(function (){
                    doReviewsSlider($reviewsWrap, sliderData);
                });
            }

            $reviewsTabHead.find('.reviews-tab-radio input').on('change', function (){
                let selectSliderMode = $(this).val();
                let sliderData = getSliderData(selectSliderMode);
                doReviewsSlider($reviewsWrap, sliderData);
            });

        });
    }
}());



$(document).on('click', '.u-video-play', function (e) {
    e.preventDefault();
    let $btnVideo = $(this);
    let $blockVideo = $btnVideo.closest('.u-video-block').eq(0);
    let $iframeVideo = $blockVideo.find('iframe').eq(0);
    $blockVideo.addClass('video-active');

    stopVideoAll($iframeVideo);
    playVideo($iframeVideo);
});

$(document).on('click', '.lg-item, .btn-get-modal', function (e) {
    stopVideoAll();
});

function playVideo($iframe) {
    $iframe.closest('.u-video-block').addClass('video-active');
    let $iframeBlock = $iframe.get(0);
    let  contentWindow = $iframeBlock.contentWindow;
    contentWindow.postMessage('{"event": "command", "func": "playVideo", "args": ""}', "*");
}
function stopVideo($iframeList) {
    let videoCount = $iframeList.length;
    for (let i = 0; i < videoCount; i++) {
        $iframeList.eq(i).closest('.u-video-block').removeClass('video-active');
        let $iframeBlock = $iframe.get(i);
        let  contentWindow = $iframeBlock.contentWindow;
        contentWindow.postMessage('{"event": "command", "func": "stopVideo", "args": ""}', "*");
    }
}

function stopVideoAll($exceptionVideoIframe = null) {
    let $allVideos = $(document).find('.u-video-block iframe');
    if ($exceptionVideoIframe !== null && $exceptionVideoIframe.length > 0) {
        $allVideos = $allVideos.not($exceptionVideoIframe);
    }

    if ($allVideos.length > 0) {
        let videoListCount = $allVideos.length;
        for (let i = 0; i < videoListCount; i++) {
            $allVideos.eq(i).closest('.u-video-block').removeClass('video-active');
            let $iframeBlock = $allVideos.get(i);
            let  contentWindow = $iframeBlock.contentWindow;
            contentWindow.postMessage('{"event": "command", "func": "stopVideo", "args": ""}', "*");
        }
    }

}





