
(function () {
    let mapAreaList = {
        'kamchatskij_kraj' : {
            'title' : 'Камчатский край',
            'icon' : 'img/region/kamchatskij_kraj.png',
            'list' : [
                'Камчатский театр драмы и комедии',
                'Камчатский театр кукол',
            ]
        },
        'respublika_saha_yakutiya' : {
            'title' : 'Республика Саха (Якутия)',
            'icon' : 'img/region/respublika_saha_yakutiya.png',
            'list' : [
                'Государственная филармония Республики Саха (Якутия)',
            ]
        },
        'amurskaya_oblast' : {
            'title' : 'Амурская область',
            'icon' : 'img/region/amurskaya_oblast.png',
            'list' : [
                'Амурский областной театр драмы',
                'Амурская областная филармония',
                'и другие учреждения',
            ]
        },
        'zabajkalskij_kraj' : {
            'title' : 'Забайкальский край',
            'icon' : 'img/region/zabajkalskij_kraj.png',
            'list' : [
                'Забайкальская краевая филармония им. О. Лундстрема',
            ]
        },
        'krasnoyarskij_kraj' : {
            'title' : 'Красноярский край',
            'icon' : 'img/region/krasnoyarskij_kraj.png',
            'list' : [
                'Канский драматический театр',
            ]
        },
        'kemerovskaya_oblast' : {
            'title' : 'Кемеровская область',
            'icon' : 'img/region/kemerovskaya_oblast.png',
            'list' : [
                'Новокузнецкий драматический театр',
                'и другие учреждения.',
            ]
        },
        'yamalo_neneckij_avtonomnyj_okrug' : {
            'title' : 'Ямало-Ненецкий автономный округ',
            'icon' : 'img/region/yamalo_neneckij_avtonomnyj_okrug.png',
            'list' : [
                'Культурно-деловой центр г. Салехард',
                'и другие учреждения',
            ]
        },
        'omskaya_oblast' : {
            'title' : 'Омская область',
            'icon' : 'img/region/omskaya_oblast.png',
            'list' : [
                'Омский драматический театр «Галёрка»',
            ]
        },
        'kurganskaya_oblast' : {
            'title' : 'Курганская область',
            'icon' : 'img/region/kurganskaya_oblast.png',
            'list' : [
                'Курганский театр кукол "Гулливер"',
            ]
        },
        'chelyabinskaya_oblast' : {
            'title' : 'Челябинская область',
            'icon' : 'img/region/chelyabinskaya_oblast.png',
            'list' : [
                'Городской дом культуры г. Южноуральск',
                'и другие учреждения',
            ]
        },
        'udmurtskaya_respublika' : {
            'title' : 'Удмуртская республика',
            'icon' : 'img/region/udmurtskaya_respublika.png',
            'list' : [
                'Национальный театр Удмуртской Республики',
                'Государственный театр кукол Удмуртской Республики',
                'и другие учреждения',
            ]
        },
        'respublika_komi' : {
            'title' : 'Республика Коми',
            'icon' : 'img/region/respublika_komi.png',
            'list' : [
                'Академический театр драмы им. В. Савина',
                'Коми республиканская филармония',
                'и другие учреждения',
            ]
        },
        'arhangelskaya_oblast' : {
            'title' : 'Архангельская область',
            'icon' : 'img/region/arhangelskaya_oblast.png',
            'list' : [
                'Архангельский театр драмы им. М. В. Ломоносова',
                'Поморская филармония',
                'и другие учреждения',
            ]
        },
        'murmanskaya_oblast' : {
            'title' : 'Мурманская область',
            'icon' : 'img/region/murmanskaya_oblast.png',
            'list' : [
                'Мурманский областной драматический театр',
                'Мурманский областной театр кукол',
                'и другие учреждения',
            ]
        },
        'leningradskaya_oblast' : {
            'title' : 'Ленинградская область',
            'icon' : 'img/region/leningradskaya_oblast.png',
            'list' : [
                'Дом ученых им. М. Горького Российской академии наук ',
                'и другие учреждения',
            ]
        },
        'tverskaya_oblast' : {
            'title' : 'Тверская область',
            'icon' : 'img/region/tverskaya_oblast.png',
            'list' : [
                'Тверская академическая областная филармония',
            ]
        },
        'yaroslavskaya_oblast' : {
            'title' : 'Ярославская область',
            'icon' : 'img/region/yaroslavskaya_oblast.png',
            'list' : [
                'Ярославский государственный театр кукол',
                'Рыбинский драматический театр',
                'и другие учреждения',
            ]
        },
        'kostromskaya_oblast' : {
            'title' : 'Костромская область',
            'icon' : 'img/region/kostromskaya_oblast.png',
            'list' : [
                'Костромской драматический театр им. Островского',
                'Государственная филармония Костромской области',
                'и другие учреждения',
            ]
        },
        'nizhegorodskaya_oblast' : {
            'title' : 'Нижегородская область',
            'icon' : 'img/region/nizhegorodskaya_oblast.png',
            'list' : [
                'Нижегородский независимый "Театр на Счастливой"',
            ]
        },
        'respublika_chuvashiya' : {
            'title' : 'Республика Чувашия',
            'icon' : 'img/region/respublika_chuvashiya.png',
            'list' : [
                'Чувашский драматический театр имени К. В. Иванова',
                'Чувашский государственный театр кукол',
            ]
        },
        'ulyanovskaya_oblast' : {
            'title' : 'Ульяновская область',
            'icon' : 'img/region/ulyanovskaya_oblast.png',
            'list' : [
                'Ульяновская филармония',
                'и другие учреждения',
            ]
        },
        'samarskaya_oblast' : {
            'title' : 'Самарская область',
            'icon' : 'img/region/samarskaya_oblast.png',
            'list' : [
                'Сызранский драматический театр им. А.Толстого',
                'и другие учреждения',
            ]
        },
        'saratovskaya_oblast' : {
            'title' : 'Саратовская область',
            'icon' : 'img/region/saratovskaya_oblast.png',
            'list' : [
                'Драматический театр-студия "Подмостки"',
            ]
        },
        'volgogradskaya_oblast' : {
            'title' : 'Волгоградская область',
            'icon' : 'img/region/volgogradskaya_oblast.png',
            'list' : [
                'Волгоградский ТЮЗ',
                'Волгоградский областной театр кукол',
            ]
        },
        'rostovskaya_oblast' : {
            'title' : 'Ростовская область',
            'icon' : 'img/region/rostovskaya_oblast.png',
            'list' : [
                'Таганрогский камерный театр',
            ]
        },
        'krasnodarskij_kraj' : {
            'title' : 'Краснодарский край',
            'icon' : 'img/region/krasnodarskij_kraj.png',
            'list' : [
                'Армавирский театр драмы и комедии',
                'Театр Старого парка (Геленджик)',
            ]
        },
        'belgorodskaya_oblast' : {
            'title' : 'Белгородская область',
            'icon' : 'img/region/belgorodskaya_oblast.png',
            'list' : [
                'Белгородский драматический театр им. М. С. Щепкина',
                'Белгородская государственная филармония',
                'и другие учреждения',
            ]
        },

        'bryanskaya_oblast' : {
            'title' : 'Брянская область',
            'icon' : 'img/region/bryanskaya_oblast.png',
            'list' : [
                'Брянский областной театр драмы им. А. К. Толстого',
                'Брянская областная филармония',
                'и другие учреждения',
            ]
        },
        'vologodskaya_oblast' : {
            'title' : 'Вологодская область',
            'icon' : 'img/region/vologodskaya_oblast.png',
            'list' : [
                'Вологодский областной театр юного зрителя',
                'Череповецкий камерный театр',
                'и другие учреждения',
            ]
        },
        'voronezhskaya_oblast' : {
            'title' : 'Воронежская область',
            'icon' : 'img/region/voronezhskaya_oblast.png',
            'list' : [
                'Воронежский академический театр драмы имени А. Кольцова',
                'и другие учреждения',
            ]
        },
        'kaluzhskaya_oblast' : {
            'title' : 'Калужская область',
            'icon' : 'img/region/kaluzhskaya_oblast.png',
            'list' : [
                'Калужский областной драматический театр',
                'Калужский театр кукол',
                'и другие учреждения',
            ]
        },
        'magadanskaya_oblast' : {
            'title' : 'Магаданская область',
            'icon' : 'img/region/magadanskaya_oblast.png',
            'list' : [
                'Магаданский областной театр кукол',
                'Магаданская областная филармония',
            ]
        },
        'moskovskaya_oblast' : {
            'title' : 'Московская область',
            'icon' : 'img/region/moskovskaya_oblast.png',
            'list' : [
                'КДЦ "Зимний театр"',
                'Музыкальный театр Натальи Колдашовой',
            ]
        },
        'orlovskaya_oblast' : {
            'title' : 'Орловская область',
            'icon' : 'img/region/orlovskaya_oblast.png',
            'list' : [
                'Орловский государственный театр "Свободное пространство"',
                'Орловская государственная филармония',
            ]
        },
        'respublika_krym' : {
            'title' : 'Республика Крым',
            'icon' : 'img/region/respublika_krym.png',
            'list' : [
                'Севастопольский русский драматический театр имени А. В. Луначарского',
                'Крымская государственная филармония',
                'и другие учреждения',
            ]
        },
        'tulskaya_oblast' : {
            'title' : 'Тульская область',
            'icon' : 'img/region/tulskaya_oblast.png',
            'list' : [
                'Тульский областной театр юного зрителя',
                'Тульская областная филармония',
                'и другие учреждения',
            ]
        },
        'kirovskaya_oblast' : {
            'title' : 'Кировская область',
            'icon' : 'img/region/kirovskaya_oblast.png',
            'list' : [
                'Кировский областной драматический театр им. С.М. Кирова',
            ]
        },
    };

    if ($('.geo-map-area').length) {
        $('.geo-map-area').on('click', 'path', function (){
            if (!isGeoMapMobile()) {
                return 0;
            }
            let $this = $(this);
            let $geoMapAreaUnit = $this.parent('.geo-map-area-unit');
            if ($geoMapAreaUnit.length) {
                let region = $geoMapAreaUnit.attr('data-region');
                $('#geo-label-region .geo-label-container').eq(0).html(createRegionHtml(mapAreaList, region));
                let setTime = setTimeout(function () {
                    center_modal("#geo-label-region");
                    clearTimeout(setTime);
                }, 30)
                return true;
            }
            center_modal("#geo-label-no-region");
            return true;
        });

        $(document).on('mouseover', '.geo-map-area path', function (){
            if (isGeoMapMobile()) {
                return 0;
            }

            let $area = $(this);
            let regionKey = getKeyRegion ($area);
            $('.geo-map-area path').removeClass('area-active');
            $area.addClass('area-active');

            if (regionKey !== null) {
                let $geoLabel =  $('#geo-label-unit-block');
                $geoLabel.find('.geo-label-region').eq(0).html(createRegionHtml(mapAreaList, regionKey));
                setPositionGeoLabel($area, $geoLabel);
                showGeoLabel($geoLabel);
            } else {
                let $geoLabelNoRegion =  $('#geo-label-unit-block-no-region');
                setPositionGeoLabel($area, $geoLabelNoRegion);
                showGeoLabel($geoLabelNoRegion);
            }
        });

        $(document).on('mouseout', '.geo-map-area path', function (evt){
            if (isGeoMapMobile()) {
                return 0;
            }
            let $relatedTarget = $(evt.relatedTarget);
            if ($relatedTarget.closest('.geo-label-unit-block').length > 0) {
                return 0;
            }
            $(this).removeClass('area-active');
            hideGeoLabels();
        });
        $(document).on('mouseleave', ' .geo-label-unit-block', function (evt){
            if (isGeoMapMobile()) {
                return 0;
            }
            $('.geo-map-area path').removeClass('area-active');
            hideGeoLabels();
        });
    }

    function createRegionHtml(mapAreaList, region) {
        let title = mapAreaList[region]['title'];
        let icon = mapAreaList[region]['icon'];
        let orgList = mapAreaList[region]['list'];
        let orgListRow = '';

        if (orgList.length) {
            orgList = orgList.map(function callback(currentValue) {
                return '<li>' + currentValue + '</li>';
            });
            orgListRow =  orgList.join(" ");
            orgListRow = '<ul>' + orgListRow + '</ul>';
        }

        let regionHtml = `
    <div class="geo-label-name-box">
        <div class="icon"><img src="${icon}" alt=""></div>
        <div class="text">${title}</div>
    </div>    
    
    <div class="geo-label-content">
        <p class="u-subtitle">Уже подключены:</p>
        ${orgListRow}
    </div>
    `;
        regionHtml += ` <div class="btn-wrap">
            <a href="javascript:void(0)" class="btn-user  btn-get-modal  btn" data-id="#modal-send-form" data-source="Присоединиться"><span class="btn-inner">Присоединиться</span></a>
        </div>`;



        return regionHtml;
    }

    function getCoordsCenterArea($elem) {
        let $box = $elem.get(0).getBoundingClientRect();
        return {
            top: parseInt($box.top + pageYOffset + parseInt($box.height, 10) / 2, 10),
            left: parseInt($box.left + pageXOffset + parseInt($box.width, 10) / 2, 10),
        };
    }

    function setPositionGeoLabel($area, $geoLabel) {
        let pos = getCoordsCenterArea($area);
        let $geoLabelWidth = parseInt($geoLabel.outerWidth(), 10);
        let screenWidth = window.screen.width;

        if (pos.left + $geoLabelWidth > screenWidth) {
            $geoLabel.addClass('reverse-position');
        } else {
            $geoLabel.removeClass('reverse-position');
        }

        $geoLabel.css({
            'top' : pos.top + 'px',
            'left' : pos.left + 'px',
        });
    }

    function showGeoLabel($geoLabel) {
        $geoLabel.removeClass('geo-label-hidden');
    }
    function hideGeoLabels() {
        $('.geo-label-unit-block').addClass('geo-label-hidden');
    }

    function getKeyRegion ($elem) {
        let $areaUnit = $elem.closest('.geo-map-area-unit[data-region]');
        let regionKey = null;
        if ($areaUnit.length > 0 && $areaUnit.attr('data-region')) {
            regionKey = $areaUnit.attr('data-region');
        }
        return regionKey;
    }

    function isGeoMapMobile() {
        if(window.matchMedia("(max-width: 800px)").matches) {
            return true;
        }
        return false;
    }

})();




function is_visible(selector){
    let w_top, e_top, w_height, e_height, e_kg, show = true;
    w_top = $(window).scrollTop();
    e_top = $(selector).offset().top;
    w_height = $(window).height();
    e_kg =  w_height*0.5;
    e_height = $(selector).outerHeight();
    if ((w_top + w_height) > e_top + e_kg && w_top < (e_top + e_height - e_kg)) return 1;
    else  return 0;
}

function animateGeoMap (evt) {
    let geoMapContainer = $('#geo-map-container');
    if(geoMapContainer.length){
        if (is_visible('#geo-map-container')) {
            let $geoMapContourRegionList = $('#geo-map-contour .contour-area-region');
            let $geoMapContourRegionCount = $geoMapContourRegionList.length;

            let i = 0;
            let setTimeAnim = setInterval(function (){
                if (i < $geoMapContourRegionCount) {
                    $geoMapContourRegionList.eq(i).addClass('fill-active');
                    i++;
                } else {
                    clearInterval(setTimeAnim);
                }

            }, 70)

            document.removeEventListener("DOMContentLoaded", animateGeoMap);
            window.removeEventListener("resize", animateGeoMap);
            window.removeEventListener("scroll", animateGeoMap);

        }
    }
}

document.addEventListener("DOMContentLoaded", animateGeoMap);
window.addEventListener("resize", animateGeoMap);
window.addEventListener("scroll", animateGeoMap);
