/**
 * menu
 */
function showMenu() {
    $('html').eq(0).addClass('menu-active');
}
function hideMenu() {
    $('html').eq(0).removeClass('menu-active');
}

function doMenuLogic() {
    $(document).on('click', '.btn-menu', function (e) {
        showMenu();
    });

    $(document).on('click', function (e) {
        let eTarget = $(e.target);
        if(eTarget.closest('.main-menu-block').length > 0 || eTarget.closest('.btn-menu').length > 0) {
            return false;
        }
        hideMenu();
    });

    $(document).on('click', '.main-menu-block', function (e) {
        let eTarget = $(e.target);
        if(eTarget.closest('.btn-menu-close') || eTarget.closest('.menu-list > li > a').length > 0) {
            hideMenu();
        }
    });
}

/**
 * scrollbar
 */

function doScrollBar() {
    let scrollbar_wrap = $('.scrollbar-wrap');
    if (scrollbar_wrap.length) {
        scrollbar_wrap.scrollbar({
            'autoScrollSize' : true,
            'ignoreMobile' : false,
            'ignoreOverlay' : true,
        });
    }
}

/**
 * lightGallery
 */

function doLightGallery() {
    let lg_box;
    lg_box = $('.lg-box');
    if(lg_box.length) {
        lg_box.lightGallery({
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            selector: '*:not(.slick-clone) .lg-item',
            download: false,
            youtubePlayerParams: {
                modestBranding: 1,
                showInfo: 0,
                rel: 0,
                controls: 1
            }
        });
    }
}

function doWow() {
    let wow = new WOW(
        {
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 150,
            mobile: false,
            live: true
        }
    );
    wow.init();
}

function doSmoothScrollingByBtn() {
    $(document).on('click', '.btn-go', function (e) {
        let dataGo = $(this).attr('data-go');
        let $obj = $(dataGo);
        if ($obj.length) {
            e.preventDefault();
            $('html, body').animate({scrollTop: $obj.offset().top}, 1000);
        }
    });

}


/**
 * Лид магнит / cookie
 */

function has_attr (attr) {
    return (typeof attr !== typeof undefined && attr !== false);
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function last_time() {
    var date, date2;
    date = new Date();
    date2 = new Date (date.getFullYear(), date.getMonth(), date.getDate()+1, 0,0,0);
    date2 = date2.toDateString();
    return (date2);
}

function doLeadMagnet() {
    let mouse_Y = 0, lead_flag = 0;
    $(document).bind('mousemove', function(e) {
        mouse_Y = e.pageY;
    });
    $(document).on('mouseleave', function(){
        if(mouse_Y <= 100) {
            lead_func();
        }
    });

    function lead_func () {
        if (lead_flag || $('html.show_modal').length > 0) return 0;
        if(getCookie('user_send') != 1) {
            modal_overlay.fadeOut(200);
            center_modal("#modal-lead-magnet");
            lead_flag = 1;
        }
    }
}


/**
 * маска телефона
 */

function doMaskPhone() {
    let input_phone = $('input[type="tel"]');
    if(input_phone.length) {
        input_phone.mask('+7(000) 000-00-00');
        input_phone.on('focus', function () {
            if ($(this).val().length === 0) {
                $(this).val('+7(');
            }
        });
    }
    $(document).on('blur', 'input[type=tel]', function(){
        var $this = $(this);
        if (  $this.val().length !== 17) {
            $this.val('');
        }
    });
}

/**
 * Работа с формой
 */

$(document).on('focus change', 'input, textarea', function(){
    $(this).removeClass('invalid');
});

$(".send-form").submit(function () {
    var $this, form_data;
    $this = $(this);
    form_data = new FormData($this[0]);
    if (is_empty($this)) {
        $.ajax({
            type: "POST",
            url: "php/mail.php",
            data: form_data,
            contentType:false,
            processData:false,
            success: function (res) {
                $this.trigger('reset');
                modal_to_close('.modal_wrapper');
                center_modal('#modal-final-send');
                document.cookie = "user_send=1; expires="+ last_time() +"; path=/; ";
            }
        });
    }
    return false;
});

function is_empty(elem) {
    var mas, objects;
    objects = elem.find('.req');
    mas = [];
    objects.each(function () {
        var $this = $(this);
        if ($this.val().length === 0 || !$this.val().replace(/\s+/g, '')) {
            $this.addClass('invalid');
            mas.push("0");
        } else if (($this.attr('type') === "checkbox") && !$this.prop('checked')) {
            $this.addClass('invalid');
            mas.push("0");
        } else if ($this.attr('type')=== "tel" && $this.val().length !== 17) {
            $this.addClass('invalid');
            mas.push("0");
        }
        else{
            $this.removeClass('invalid');
        }
    });
    if (mas.length === 0) return 1;
    else return 0;
}


$(document).ready(function () {
    doMenuLogic();
    doScrollBar();
    doLightGallery();
    doWow();
    doSmoothScrollingByBtn();
    doMaskPhone();

/*    let leadMagnetSetTime = setTimeout(function () {
        doLeadMagnet();
        clearTimeout(leadMagnetSetTime);
    }, 2500);*/
});